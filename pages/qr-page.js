// imports
import React, { useState, useEffect } from "react";
import { NextSeo } from "next-seo";
import Link from 'next/link'
import Router from 'next/router';
import axios from "axios";
// components
import SocialContainer from "@components/producer-social-container";
import { Button } from "@components/button";
import SocialButton from '@components/social/SocialButton'
import { jwtEncodeData } from "@components/jwt-verify";

// styles
import "@styles/qrcode.scss";

// component
const QRPage = ({ qrID }) => {

	const [showPageType, setShowPageType] = useState('');
	const [pointsCollected, setPointsCollected] = useState('0');

	// add body class
	useEffect(() => {
		document.getElementsByTagName("body")[0].className = "qr-page-body";
		jQuery(".profile").hide();
		jQuery(".sign-in").hide();
		jQuery(".share-icon").hide();
		jQuery(".menu-icon").replaceWith('<a style="position: initial;" class="close-wrapper" href="/gamification"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAkCAYAAAAOwvOmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NkIxQTNGRTNFMUZCMTFFOTgwOTNBQkI3M0I1OEE2RDQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NkIxQTNGRTRFMUZCMTFFOTgwOTNBQkI3M0I1OEE2RDQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo2QjFBM0ZFMUUxRkIxMUU5ODA5M0FCQjczQjU4QTZENCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo2QjFBM0ZFMkUxRkIxMUU5ODA5M0FCQjczQjU4QTZENCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpHJaM8AAADDSURBVHja7NhNCsMgEAVgLT1Xppvkpu3luukNrJAphNC0znsGXLyBIVmofPiHmkuNNFhc/ZsHMpVLGjCEEkoooUjUveYcKD95ncD2uUYK5K3ms+bcUHbyskug/YKgWmEIiEL9g6EgGnUEY0BdUHsYC+qG+sBengvZVrejSzn4B1vje8o2QxbZLk4bPvsyh4yEUSj7MakZGIyyhlWGwiCUBZY9AoNQj+CyN6/TjMp+79MVSydPoYQSSqgTX/KGemJ8CzAAefh24lRHruEAAAAASUVORK5CYII=" alt="close button" class="close-btn"></a>');
		async function checkUserLogin() {
			const response		= await axios(process.env.REACT_APP_API_URL + 'check-login?date=' + Math.floor(Date.now() / 1000))
			const auth_response	= response.data

			if(auth_response.is_logged_in == 1)
			{
				addPointsInUserAccount()
			}
			else
			{
				setShowPageType('login_section');
			}
		}

		checkUserLogin();
	}, [0]);

	// render
	return (
		<div className="container qr-container">
			<NextSeo
				title="The Great Singapore Replay: Season 2: QR Page"
				description=""
			/>
			<div className="inner-container">

				{ showPageType == 'login_section' && (
					<div className="row header-wrapper">
						<div className="col-lg-12 col-md-12">
							<h3 className="header">SIGN IN TO RACK UP RIFFS	</h3>
							<div className="login-container">
								<SocialButton
									provider='facebook'
									appId={process.env.REACT_APP_FACEBOOK_APP_ID}
									onLoginSuccess={handleSocialLogin}
									onLoginFailure={handleSocialLoginFailure}
									classes='login-button facebook'
									>
									<img
										src={require("../assets/static/images/navigation/btn_icon_fb.png")}
										alt="facebook login"
									/>
									<span>CONTINUE WITH FACEBOOK</span>
								</SocialButton>

								<SocialButton
									provider='google'
									appId={process.env.REACT_APP_GOOGLE_APP_ID}
									onLoginSuccess={handleSocialLogin}
									onLoginFailure={handleSocialLoginFailure}
									classes='login-button google'
									>
									<img
										src={require("../assets/static/images/navigation/btn_icon_google.png")}
										alt="google login"
									/>
									<span>CONTINUE WITH GOOGLE</span>
								</SocialButton>
							</div>
							<p>MAYBE NOT NOW, BUT TELL ME MORE!</p>
							<div className="bottom-wrapper text-center">
								<a href="/artists" className="center-it">About Aspiring Music Talents</a>
							</div>
						</div>
					</div>
				)}

				{ showPageType == 'reward_collected' && (
					<div className="row header-wrapper">
						<div className="col-lg-12 col-md-12">
							<div className="header-image">
								<img
									src={require("../assets/static/images/qrcode/img_boombox_tipping@2x.png")}
									alt="boom box"
								/>
							</div>
							<h3 className="header earned-header">HERE'S <span className="yellow-text heading">{pointsCollected} RIFFS</span> TO ADD TO YOUR SCORESHEET!</h3>
							<p>Complete more activities to rack up more Riffs and to win amazing prizes.</p>
							<div className="bottom-wrapper">
								<a className="button raised long-btn postlogin text-center" href="/gamification">
									CONTINUE
								</a>
							</div>
						</div>
					</div>
				)}

				{ showPageType == 'reward_already_collected' && (
					<div className="row header-wrapper">
						<div className="col-lg-12 col-md-12">
							<h3 className="header">THESE RIFFS ARE ALREADY ON YOUR SCORESHEET!</h3>
							<p>We love your enthusiasm!</p>
							<p>Complete a different activity to earn more Riffs. Stardom is just around the corner!</p>
							<div className="bottom-wrapper">
								<a className="button raised long-btn postlogin text-center" href="/gamification">
									CONTINUE
								</a>
							</div>
						</div>
					</div>
				)}

				{ showPageType == 'something_went_wrng' && (
					<div className="row header-wrapper">
						<div className="col-lg-12 col-md-12">
							<h3 className="header">
								OOPS..
								<br />
								STRUCK A WRONG CHORD!
							</h3>
							<p>SOMETHING WENT WRONG. PLEASE TRY AGAIN.</p>
							<div className="bottom-wrapper">
								<a className="button raised long-btn postlogin text-center" href="/">
									BACK TO HOME
								</a>
							</div>
						</div>
					</div>
				)}
			</div>
		</div>
	);

	function addPointsInUserAccount()
	{
		var request = {
			"event_type"    : "qr_scanned",
			"action_element": qrID
		}

		axios(process.env.REACT_APP_API_URL + 'add-points?date=' + Math.floor(Date.now() / 1000), {
			method: 'post',
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			data: jwtEncodeData(request)
		}).then((response) => {
			var add_point_response = response.data;

			if(add_point_response.status == 1)
			{
				setShowPageType('reward_collected');
				setPointsCollected(add_point_response.points);
				$('#wriffs').html(parseInt($('#wriffs').html()) + add_point_response.points);
				triggerGTMEvent('qr-points-earned-view');
			}
			else if(add_point_response.status == 0)
			{
				setShowPageType('something_went_wrng');
				triggerGTMEvent('qr-something-went-wrong-view');
			}
			else if(add_point_response.status == 2)
			{
				setShowPageType('reward_already_collected');
				triggerGTMEvent('qr-points-already-collected-view');
			}
		});
	}

	function triggerGTMEvent(eventName){
		window.dataLayer = window.dataLayer || []; window.dataLayer.push({'event': eventName});
	}

	function handleSocialLoginFailure(err){
		setShowPageType('something_went_wrng');
	}

	function handleSocialLogin(user){
		var data = {
			"social_type": user._provider,
			"social_id": user._profile.id,
			"email_id": user._profile.email,
			"full_name": user._profile.name
		}
		axios(process.env.REACT_APP_API_URL + 'login?date=' + Math.floor(Date.now() / 1000), {
			method: 'post',
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			data: jwtEncodeData(data)
		}).then((res) => {

			axios(process.env.REACT_APP_API_URL + 'check-login?date=' + Math.floor(Date.now() / 1000)).then((response) => {
				var auth_response = response.data;

				if(auth_response.is_logged_in == 1){
					// addPointsInUserAccount();
					Router.reload('/');
				}else{
					setShowPageType('something_went_wrng');
				}

			});
		});
	}
};

// initial props
QRPage.getInitialProps = async ({ req, query }) => {
	return {qrID: query.guid};
};

export default QRPage;
