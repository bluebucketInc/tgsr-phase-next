// imports
import React, { useEffect, Fragment, useState } from "react";
import Link from "next/link";
import { NextSeo } from "next-seo";
import fetch from "isomorphic-unfetch";
import { findWhere } from "underscore";
import { toast } from "react-toastify";
import axios from "axios";
// component
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { jwtEncodeData } from "@components/jwt-verify";
import Countdown from "@components/countdown"
import { Boombox } from "@components/hidden-boombox";
import { Button } from "@components/button";
import { PointsToast, AlertToast } from "@components/toast";
// styles
import "@styles/festival.scss";

// component
const Festival = ({ bbFlag, bbID, isReal }) => {

	// share url
	const [shareURL, setshareURL] = useState("");
	const [titleText, setTitleText] = useState("You're invited to the");

	// add body class
	useEffect(() => {
		document.getElementsByTagName("body")[0].className = "festival-page";
		getRsvp();
		// set url
		setshareURL(window.location.href);
	}, [0]);

	const [isChecked, setAge] = useState(false);
	var noPointer = {
        'pointerEvents': 'none',
        'display': 'none'
    }

    // artists
    const artists = [
		{
			name: "RENE",
			page: "rene",
		},
		{
			name: "Marcus 李俊纬",
			page: "marcus",
		},
		{
			name: "aeriqah",
			page: "aeriqah",
		},
		{
			name: "Marian Carmel",
			page: "marian-carmel",
		},
		{
			name: "NAMIE",
			page: "namie",
		},
		{
			name: "Zalelo",
			page: "zalelo",
		},
		{
			name: "whirring",
			page: "whirring",
		},
		{
			name: "The Cold Cut Duo 雙節奏",
			page: "the-cold-cut-duo",
		},
		{
			name: "Fingerfunk",
			page: "fingerfunk",
		},
		{
			name: "ABANGSAPAU",
			page: "abangsapau",
		}
	];
    
    var blankStyle = {};
	// render
	return (
		<Fragment>
			<div className="mentors-spotlight"><img src="clientlibs/images/mentors/mentor-spotlight.png" /></div>
			<div className="events-page body-contents festival-container">
				<div className="masthead-container">
					<img className="for-desktop" src={require("@images/festival/tgsr_festival_main.png")} alt="Festival Page"/>
					<img className="for-mobile" src={require("@images/festival/img_festival_main_mobile.png")} alt="Festival Page"/>
					<Countdown date={`2020-01-18T17:00:00`} />
					<p className="masthead-desc align-center">Singapore’s next generation of emerging music acts will be showcasing their amazing talent live for one night only! 
 					</p>
					<p className="masthead-desc second align-center">Catch our TGSR artists during Replay Hour at 9pm, as they come together to perform their original 'Made-in-SG' songs for the first time ever. 
					</p>
					<p className="masthead-desc second align-center">The countdown to the Showcase Concert has begun, so grab your buddies and start practising your dance moves!
					</p>
					
					{/*<Button href={'#rsvp-section'}>RSVP</Button>*/}
					
				</div>
				
			</div>
			<div className="events-page body-contents festival-container">
				<div className="events-container">
					<div className="step-container">
						<div className="header step-header text-center texture-bg">
							<h4 className="text-uppercase align-center">SHOWCASE CONCERT </h4>
							<h2 className="text-uppercase align-center">ARTIST LINE-UP </h2>
						</div>
						<div className="mic-icon-container">
							<div className="mic-top">
								<img src="clientlibs/images/events/mic.png" />
							</div>
						</div>
						<div className="events-timeline">
							<div className="past-timeline active">
								<ul>
									{
										artists.map(i =>
											<li
												key={i.page}
												data-aos="fade-up"
												data-aos-duration="600"
												data-aos-offset="350"
												className="aos-init"
											>
												<div className="content">
													<h5 className="title-blue"><a href={`/artists/${i.page}`}>{i.name}</a></h5>
													
												</div>
											</li>
										)
									}
								</ul>
							</div>
						</div>
					</div>
					<div className="step-container container">
						<div className="header step-header text-center">
							<h4 className="text-uppercase align-center">ARE YOU READY?</h4>
							<h2 className="text-uppercase align-center">Showcase Highlights</h2>
						</div>
						<div className="activity-container">
							
							<div className="item">
								<div className="highlight-img">
									<img
										src={require("@images/festival/concert.png")}
										alt="Activity"
									/>
								</div>
								<div className="highlight-text">
									<h4 className="text-uppercase">Replay Hour</h4>
									<p>
										Catch live performances of original ‘Made-in-SG’ songs by our emerging music acts
									</p>
								</div>
							</div>

							<div className="item">
								<div className="highlight-img">
									<img
										src={require("@images/festival/replay.png")}
										alt="Activity"
									/>
								</div>
								<div className="highlight-text">
									<h4 className="text-uppercase">Early Bird Giveaways</h4>
									<p>
										Be one of the first 100 to arrive and snag early bird limited edition TGSR tote bags
									</p>
								</div>
							</div>
							<div className="item">
								<div className="highlight-img">
									<img
										src={require("@images/festival/boombox.png")}
										alt="Activity"
									/>
								</div>
								<div className="highlight-text">
									<h4 className="text-uppercase">Mega Boombox</h4>
									<p>
										Create your own mashup and explore the source of inspiration behind the 10 songs. Check out our mini exhibition charting their creative journey
									</p>
								</div>
							</div>
							<div className="item">
								<div className="highlight-img">
									<img
										src={require("@images/festival/laugh.png")}
										alt="Activity"
									/>
								</div>
								<div className="highlight-text">
									<h4 className="text-uppercase">Tasty Treats</h4>
									<p>
										Tantalise your taste buds with familiar local snacks at our food stalls
									</p>
								</div>
							</div>
						</div>
					</div>
					<div className="step-container text-center container" id="rsvp-section">
						<div className="header step-header text-center rsvp-header">
							<h4 className="text-uppercase align-center">{titleText}</h4>
							<h2 className="text-uppercase align-center text-yellow">SHOWCASE CONCERT!</h2>
						</div>
						<div className="event-details">
							<div className="date">
								<img className="logo" src={require("@images/festival/icon_add to calender@2x.png")} alt="calendar"/>
								<h5 className="text-uppercase">18 JAN 2020, 5:00PM - 10:30PM</h5>
							</div>
							<div className="location">
								<img className="logo location-logo" src={require("@images/festival/icon_location@2x.png")} alt="location"/>
								<h5 className="text-uppercase">SUPERTREE GROVE, GARDENS BY THE BAY</h5>
							</div>
							
						</div>
						<div className="inner-content" id="div1" style={{display: 'none'}}>
						<div className="event-desc">
								<p>RSVP to the Showcase Concert today to rack up 50 Riffs instantly! Hurry, click on the button below!</p>
							</div>
							{/*<FormControlLabel
		                        control={
		                            <Checkbox
		                                checked={isChecked}
		                                onChange={(e) => setAge(e.target.checked)}
		                               
		                                value="age"
		                            />
		                        }
		                        label="I can't wait to attend the Showcase Concert! "
		                    />*/}
							<div>
							<Button classes="long-btn rsvpBtn" onClick={submitRsvp}>I'M GOING!</Button>
							</div>
						</div>
						<div id="div2" style={{display: 'none'}}>
							<p className="text-center">FOMO is real! <a href="/api/tgsr.ics">Save the date</a> on your calendar and share this event with your concert buddies today!</p>
						</div>
						<div id="div3" style={{display: 'none'}}>
							<p className="text-center">You've RSVP'd to the Showcase Concert! <a href="/api/tgsr.ics">Save the date</a> on your calendar and share this event with your concert buddies today!</p>
						</div>
						<button onClick={getRsvp} id="triggerRsvp" style={{display: 'none'}}>login</button>
						<div className="inner-content">
							<p className="text-center text-uppercase social-share-title">INVITE YOUR CONCERT BUDDIES</p>
							<div className="social-share-link">
								<a
                                    href={`https://www.facebook.com/sharer.php?u=${shareURL}`}
                                    title="facebook"
                                    target="_blank"
                                >
									<img className="social-icon" src={require("@images/festival/icon_fb@2x.png")} alt="facebook"/>
								</a>
								<a
                                    href={`https://twitter.com/intent/tweet?url=${shareURL}`}
                                    title="Twitter"
                                    target="_blank"
                                >
									<img className="social-icon social-tweet" src={require("@images/festival/icon_twitter@2x.png")} alt="twitter"/>
								</a>
							</div>
						</div>
						
						<img className="boombox" src={require("@images/festival/img_boombox_graphics@2x.png")} alt="BoomBox"/>
						<img className="boombox-mobile" src={require("@images/festival/img_boombox_mobile.png")} alt="BoomBox"/>

					{ bbFlag == true && <Boombox state="earn" type={isReal} onClick={() => boomboxClicked()} bbID={bbID} /> }
					</div>
				</div>
			</div>

		</Fragment>
	);

	function getRsvp(){
		axios(process.env.REACT_APP_API_URL + 'get-rsvp?date=' + Math.floor(Date.now() / 1000)).then((res) => {
			var response = res.data;
			if(response.status == 1){
				setTitleText("See you at the");
				$('#div1').css('display','none');
				$('#div3').css('display','block');
			}
			else 
			{
				setTitleText("You're invited to the");
				$('#div1').css('display','block');
				$('#div3').css('display','none');
			}
		});
	}

	function submitRsvp(){
		
		var request ={
			"rsvp_status"    : 'yes'
		}

		axios(process.env.REACT_APP_API_URL + 'get-rsvp?date=' + Math.floor(Date.now() / 1000)).then((res) => {
			var response = res.data;
			if(response.status != 1){
				if($('#login_type').text() == 'pre'){
					jQuery(".rsvpBtn").addClass("rsvpBtnToDo");
					$('.sign-in').trigger("click");
				}
				else{
					fetch(process.env.REACT_APP_API_URL + 'add-rsvp', {
					method: 'post',
					headers: {
						'Accept': 'application/json, text/plain, */*',
						'Content-Type': 'application/json'
					},
					body: jwtEncodeData(request)
					}).then((res) => {
						res.json().then((response) => {
							if(response.status == 1){
								$('#wriffs').html(parseInt($('#wriffs').html()) + 50);
								toast(<PointsToast points={response.points} eventMessage={response.event_msg} dynamicMessage={response.dynamic_msg} />);
								setTitleText("See you at the");
								$('#div1').hide();
								$('#div2').show();
							}
						});
					});
					
				}
			}else{
				setTitleText("See you at the");
				$('#div1').css('display','none');
				$('#div3').css('display','block');
			}
		});
	}
	
};

// initial props
Festival.getInitialProps = async ({ req, query }) => {
	// dynamicaly import producer content

	var request = {
		page_url: "festival_page"
	};

	const res = await fetch(
		process.env.REACT_APP_API_SERVER_URL + "get-page-boomboxes-list",
		{
			method: "post",
			headers: {
				Accept: "application/json, text/plain, */*",
				"Content-Type": "application/json"
			},
			body: jwtEncodeData(request)
		}
	);

	const response = await res.json();

	let bbFlag = false;
	let bbID = 0;
	let isReal = "wrong";

	if (response.status == 1) {
		var hasBoombox = findWhere(response.list, { section_id: "festival_page" });

		if (typeof hasBoombox != "undefined") {
			bbFlag = true;
			bbID = hasBoombox.boombox_id;
			isReal = hasBoombox.is_real == 1 ? "correct" : "wrong";
		}
	}

	return { bbFlag: bbFlag, bbID: bbID, isReal: isReal };
};

/*
<li
	data-aos="fade-up"
	data-aos-duration="600"
	data-aos-offset="350"
	className="aos-init"
>
	<div className="content">
		<h5 className="title-blue"><a href="/artists/abangsapau">ABANGSAPAU</a></h5>
		<div className="event-schedule">
			<div className="schedule date">8:20 PM</div>
		</div>
		
	</div>
</li> */

// producers
export default Festival;
