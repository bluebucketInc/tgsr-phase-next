// imports
import React, { useEffect } from "react";
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from "isomorphic-unfetch";
import { findWhere } from "underscore";
import { Boombox } from "@components/hidden-boombox";
import { jwtEncodeData } from "@components/jwt-verify";

// component
const Events = ({ bbFlag, bbID, isReal }) => {

	// add body class
	useEffect(() => {
		document.getElementsByTagName("body")[0].className = "events-background";
	}, [0]);

	// render
	return (
		<div className="events-page body-contents">
			<div className="events-carousel-container body-contents events-text-info">
				<div className="events-slider">
					<div className="slider-item">
						<div className="events-slider-item">
							<div className="item-content-block">
								<div className="text-block">
									<div className="text-content">
										<h5 className="title-pink">What's New</h5>
										<h3>THE  GREAT SINGAPORE REPLAY - POP-UP PERFORMANCES</h3>
										<div className="event-details">
											<p>Time to get soaked in music, as The Great Singapore Replay artists come to town in a batch of pop-up performances from 23 November to 21 December 2019! Listen to these talented acts croon and swoon you as they gear up for the TGSR showcase concert at Gardens by The Bay on 18 Jan 2020.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="events-container">
				<div className="tabs-row">
					<div className="tab-title"><a className="active" href="#" data-id="upcoming">Upcoming</a></div>
					<div className="tab-title"><a href="#" data-id="past">Past</a></div>
				</div>
				<div className="mic-icon-container">
					<div className="mic-top"><img src="clientlibs/images/events/mic.png" /></div>
				</div>
				<div className="events-timeline">
					<div className="upcoming-timeline active" data-id="upcoming"></div>
					<div className="past-timeline" data-id="past"></div>
				</div>
			</div>
			<div className="popover event-details">
				<div className="event-overlay-background"><img src="clientlibs/images/events/events-desktop-overlay.png" /></div>
				<div className="popover-container">
					<div className="event-slide container">
						<div className="row">
							<div className="event-image col-12 col-sm-12 col-md-3 col-lg-3"><img src="clientlibs/images/events/place-1.png" /></div>
							<div className="event-overlay-detail col-12 col-sm-12 col-md-9 col-lg-9">
								<div className="scroll-container details">
									<h5 className="title-pink">upcoming</h5>
									<h4 className="title-blue">The Great Singapore Replay Spin Player Comes To Town</h4>
									<div className="event-schedule">
										<div className="schedule date">12 - 14 Nov 2019</div>
										<div className="schedule place">Raffles City Shopping Center</div>
									</div>
									<div className="scroll-container details description">
										<p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button className="close-overlay">X</button>
				<footer className="body-contents">
					<div className="container-fluid">
						<div className="row">
							<div className="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3">
								<ul className="footer-list footer-left">
									<li>
										<div className="title">Presented by</div>
										<div className="partner-logo"><a className="no-line" href="https://www.temasek.com.sg/" target="_blank"><img src="clientlibs/images/global/logo_temasek_white@2x.png" /></a></div>
									</li>
								</ul>
							</div>
							<div className="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
								<div className="footer-logos-block">
									<div className="footer-logos-content"><img src="clientlibs/images/footer/footer-logo.png" /></div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>

			{ bbFlag == true && <Boombox type={isReal} onClick={() => boomboxClicked()} bbID={bbID} /> }
		</div>
	);
};

// initial props
Events.getInitialProps = async ({ req, query }) => {
	// dynamicaly import producer content

	var request ={
		"page_url": "events"
	}

	const res = await fetch(process.env.REACT_APP_API_SERVER_URL + 'get-page-boomboxes-list', {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		body: jwtEncodeData(request)
	});

	const response = await res.json();

	let bbFlag	= false;
	let bbID	= 0;
	let isReal	= 'wrong';

	if(response.status == 1) {
		var hasBoombox = findWhere(response.list, {section_id: 'events'});

		if(typeof hasBoombox != 'undefined') {
			bbFlag	= true;
			bbID	= hasBoombox.boombox_id;
			isReal	= hasBoombox.is_real == 1 ? 'correct' : 'wrong';
		}
	}

	return { bbFlag: bbFlag, bbID: bbID, isReal: isReal };
};

// producers
export default Events;
