// imports
import React, { useEffect } from "react";
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from "isomorphic-unfetch";

// component
const TimesTunes = () => {

	useEffect(() => {
        document.getElementsByTagName("footer")[0].className = "body-contents";
        $(".timeline-toggler input").trigger('click');
        $(".timeline-toggler input").trigger('click');
	}, [0]);

	// render
	return (
		<div className="body-contents">
			<div className="timeline-times-background active"><img src="clientlibs/images/timeline/times/times-background.jpg" /></div>
			<div className="timeline-tunes-background"><img src="clientlibs/images/timeline/tunes/tunes-background.jpg" /></div>
			<div className="container-fluid timeline-page">
				<div className="timeline-page-content">
					<div className="timeline-title">
						<h5>EXPERIENCE THE</h5>
						<h3>TIMES & TUNES</h3>
						<p>Discover the milestone moments of history - the iconic eras that shaped our culture and identity, and the modern musical genres that will inspire the songs to be created.</p>
					</div>
					<div className="timeline-toggler">
						<div className="toggler-content">
							<div className="times-title active">Times</div>
							<div className="toggler-button" id="buttonToggle">
								<label className="switch">
									<input type="checkbox" /><span className="slider round"></span>
								</label>
							</div>
							<div className="tunes-title">Tunes</div>
						</div>
					</div>
					<div className="times-carousel center">
						<div className="times-item">
							<div className="times-content">
								<div className="times-image"><img src="clientlibs/images/timeline/times/img_time_1300@2x.png" /></div>
								<div className="times-details">
									<h1 className="title title-blue">1300s</h1>
									<div className="description-block">
										<h5>An Island Called Temasek</h5><a className="artist-description" href="#times_0" overlay-dialog="">Read more</a>
									</div>
								</div>
							</div>
						</div>
						<div className="times-item">
							<div className="times-content">
								<div className="times-image"><img src="clientlibs/images/timeline/times/img_time_1400@2x.png" /></div>
								<div className="times-details">
									<h1 className="title title-blue">1400 - 1700</h1>
									<div className="description-block">
										<h5>The Rise Of Singapura</h5><a className="artist-description" href="#times_1" overlay-dialog="">Read more</a>
									</div>
								</div>
							</div>
						</div>
						<div className="times-item">
							<div className="times-content">
								<div className="times-image"><img src="clientlibs/images/timeline/times/img_time_1819@2x.png" /></div>
								<div className="times-details">
									<h1 className="title title-blue">1819</h1>
									<div className="description-block">
										<h5>Arrival Of The British</h5><a className="artist-description" href="#times_2" overlay-dialog="">Read more</a>
									</div>
								</div>
							</div>
						</div>
						<div className="times-item">
							<div className="times-content">
								<div className="times-image"><img src="clientlibs/images/timeline/times/img_time_1900@2x.png" /></div>
								<div className="times-details">
									<h1 className="title title-blue">1900 - 1945</h1>
									<div className="description-block">
										<h5>Pre-War Singapore</h5><a className="artist-description" href="#times_3" overlay-dialog="">Read more</a>
									</div>
								</div>
							</div>
						</div>
						<div className="times-item">
							<div className="times-content">
								<div className="times-image"><img src="clientlibs/images/timeline/times/img_time_1965@2x.png" /></div>
								<div className="times-details">
									<h1 className="title title-blue">1965 - Present</h1>
									<div className="description-block">
										<h5>Journey Into Nationhood</h5><a className="artist-description" href="#times_4" overlay-dialog="">Read more</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="tunes-carousel center">
						<div className="tunes-item">
							<div className="tunes-content">
								<div className="tunes-image"><img src="clientlibs/images/timeline/tunes/img_tunes_jazz@2x.png" /></div>
								<div className="tunes-details">
									<h1 className="title title-pink">Jazz</h1>
									<div className="description-block">
										<h5>Roots Music Of The World</h5><a href="#tunes_0" overlay-dialog="" lass="artist-description">Read more</a>
									</div>
								</div>
							</div>
						</div>
						<div className="tunes-item">
							<div className="tunes-content">
								<div className="tunes-image"><img src="clientlibs/images/timeline/tunes/img_tunes_electronic@2x.png" /></div>
								<div className="tunes-details">
									<h1 className="title title-pink">Electronic</h1>
									<div className="description-block">
										<h5>Music Of The Technological Age</h5><a href="#tunes_1" overlay-dialog="" lass="artist-description">Read more</a>
									</div>
								</div>
							</div>
						</div>
						<div className="tunes-item">
							<div className="tunes-content">
								<div className="tunes-image"><img src="clientlibs/images/timeline/tunes/img_tunes_urban@2x.png" /></div>
								<div className="tunes-details">
									<h1 className="title title-pink">Urban</h1>
									<div className="description-block">
										<h5>Soul Music From The Underground</h5><a href="#tunes_2" overlay-dialog="" lass="artist-description">Read more</a>
									</div>
								</div>
							</div>
						</div>
						<div className="tunes-item">
							<div className="tunes-content">
								<div className="tunes-image"><img src="clientlibs/images/timeline/tunes/img_tunes_pop@2x.png" /></div>
								<div className="tunes-details">
									<h1 className="title title-pink">Pop</h1>
									<div className="description-block">
										<h5>Popular With The People</h5><a href="#tunes_3" overlay-dialog="" lass="artist-description">Read more</a>
									</div>
								</div>
							</div>
						</div>
						<div className="tunes-item">
							<div className="tunes-content">
								<div className="tunes-image"><img src="clientlibs/images/timeline/tunes/img_tunes_indie@2x.png" /></div>
								<div className="tunes-details">
									<h1 className="title title-pink">Indie</h1>
									<div className="description-block">
										<h5>Independent Music</h5><a href="#tunes_4" overlay-dialog="" lass="artist-description">Read more</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="popover mentor-detail timeline-tunes-detail">
				<div className="popover-container">
					<div className="mentor-slide container" data-id="#times_0">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/times/times-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/times/img_time_1300@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-blue">1300s</h1>
									<h5>An Island Called Temasek</h5>
									<div className="description">
										<p>The small trading post of Temasek was a quiet place of respite for seafarers before continuing to more challenging waters. The trading post carried goods like sago, nutmeg and rice from mainland kingdoms and from nomadic fishermen groups. The island, while quiet, was subject to claim and political interest because of its strategic location within existing networks of trade.</p>
										<p>The tranquility was short lived, however. With the arrival of Sumatran prince Sang Nila Utama came development of the island, as well as its renaming to Singapura, as it quickly advanced from a trading outpost to a vibrant commercial hub. For the next century Temasek thrived as a key port in the region, but the prosperity ended as soon as it came when the Majapahit empire invaded and took the settlement for themselves.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#times_1">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/times/times-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/times/img_time_1400@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-blue">1400 - 1700</h1>
									<h5>The Rise Of Singapura</h5>
									<div className="description">
										<p>Singapura later became a feeder state (&quot;shahbandaria&quot;) to the Melaka Sultanate, as trade flourished through the region's new port of call. Goods came from all throughout the region, with the shabandaria even becoming a part of the famed Islamic Trade Network under the watch of one of the four Great Lords of the Melaka Court, whose warriors fiercely patrolled the Straits.</p>
										<p>Singapura later saw further increases in commerce with the growth and trade of nearby states. Spice and crop trade grew tremendously, through the lack of natural disasters and climate shifts which allowed for optimal growing conditions. Along with this, surpluses in harvests and population growth made the era one of prosperity. In the center of the prosperity was Singapura, which had likely always been a cosmopolitan point for the flow of trade and commerce.</p>
										<p>While the region experienced increasing growth, the Portuguese were continuing their crusade against Islamic trade routes, relentlessly attacking Islamic vessels on the routes and continuing southward, until they reached Melaka. Once they had taken Melaka, the Portuguese took the opportunity to regroup. While they did so, the son of the previous Sultan Mahmud of Melaka reestablished himself in Johor, inadvertently helping Singapura to thrive. This prospering attracted the unwanted attention of the Portuguese and Acehnese who sacked the port, sending it into dormancy for centuries.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#times_2">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/times/times-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/times/img_time_1819@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-blue">1819</h1>
									<h5>Arrival Of The British</h5>
									<div className="description">
										<p>As European society grew a massive appetite for spices from the Far East, the region saw an influx of powers rush to take advantage of the demand. The Spanish arrived in the Philippine Archipelago to compete with the Portuguese. The Dutch, with Europe's largest naval force, took control of Java and monopolized their spice plantations, ports and networks and routes back to Europe. The British, Danish, and French followed soon after, establishing their own respective ports of control.</p>
										<p>Sir Stamford Raffles established Singapore as the British's port, after signing a treaty with local leader Temenggong Abdul    Rahman. The treaty called for the exclusivity of trade and British protection of the area. Shortly after, Raffles set up churches and schools, a city center, roads and barracks to establish a strong starting presence for William Farquhar to administer in his stead as he departed to Bencoolen for a few years, eventually returning and taking direct control of Singapore back from Farquhar.</p>
										<p>Once he had regained control of Singapore, Raffles instituted a code of settlement for the populace, which was soon followed by laws regarding freedom of trade. As the British continued to extend their ownership over the island, a police force and magistrate were set up on British principles, turning a former trading post into a proper city.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#times_3">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/times/times-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/times/img_time_1900@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-blue">1900 - 1945</h1>
									<h5>Pre-War Singapore</h5>
									<div className="description">
										<p>The early 20th century was a prosperous time for the colony, with fortunes being made overnight. While Singapore's leading Asian businessmen were amassing great fortunes, the European community prospered, living in style and convenience.</p>
										<p>Pioneers of Singapore grew into prominence, setting up their interests on the island and engaging in acts of philanthropy to assist people of their own respective races as well as the indigenous population. Tan Kah Kee, Tan Tock Seng and Aw Boon Haw made fortunes in the trade of rubber and tin, speculation of land and in the production and sale of patent medicines. Influential businessmen like P. Govindasamy Pillai, a leading member and philanthropist in Singapore's South Indian community, took advantage of the Chettiars established finance and lending resources. Property moguls like the Alsagoffs and the Aljunieds acted as handling agents for Arab investors, further enriching the port after their forefathers Syed Omar bin Ali-Aljunied and Syed Ahmad Alsagoff settled in the Singapore port under British rule.</p>
										<p>This prosperity took a turn for the worst after the outbreak of World War II. With the Japanese Army taking over in 1942, dark times ensued. The Japanese occupation was one of the most horrific times in Singapore history.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#times_4">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/times/times-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/times/img_time_1965@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-blue">1965 - Present</h1>
									<h5>Journey Into Nationhood</h5>
									<div className="description">
										<p>After the war, Singapore regained its status as a port, just as before the war. This time however, Singapore surpassed the previous standards. Obstacles were prevalent in post war Singapore. Riots, pressure from communist ideologies, and Indonesia's Konfrontasi campaign all hindered the country's progress, but never halted it. The path to independence began with David Marshall's appeal to the British Crown. The appeal fails but the seeds were planted, and with them, Lim Yew Hock gains full governance for Singapore with Lee Kuan Yew becoming the first Prime Minister in 1959.</p>
										<p>Despite challenges and hardship, Singapore's National identity slowly takes form and becomes an independent nation on 9 August 1965, after a failed merger with Malaysia. Under the leadership of Lee Kuan Yew, Singapore has surpassed its neighbors in economic progress and infrastructure. Eventually establishing itself as Southeast Asia's most prized gem, Singapore attracted global commerce and investors to its ports, enriching the country and enabling it to push into the first world.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#tunes_0">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/tunes/tunes-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar tunes col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/tunes/img_tunes_jazz@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-pink">Jazz</h1>
									<h5>Roots Music Of The World</h5>
									<div className="description">
										<p>Originating in the African - American communities of New Orleans in the late 19th and 20th centuries, Jazz is seen by many as &quot;America's classNameical music&quot;. Since the 1920's, jazz has become recognised as a major form of musical expression, with intellectuals around the world hailing it as &quot;one of America's original art forms&quot;. Jazz as it spread throughout the world, drew on different cultures, giving rise to different original styles.</p>
										<p>Jazz was popular in Singapore in the beginning of the 20th century with the boom of sub-genres like Dixieland, big band, and bebop. Nightclubs, hotels, and cabarets were jazz hotspots in Singapore and featured not only local but also famous international musicians. Recent local titans include Jeremy Monteiro and Chok Kerong. The genre has a promising future in Singapore, with it being included in school syllabus at examinable levels.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#tunes_1">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/tunes/tunes-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar tunes col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/tunes/img_tunes_electronic@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-pink">Electronic</h1>
									<h5>Music Of The Technological Age</h5>
									<div className="description">
										<p>Electronic music is music that is made through the use of digital and electronic instruments that employ circuitry-based music technology. Electronic music largely consists of sounds produced by the theremin, synthesizer, and computer-generated sounds.</p>
										<p>Electronic music can be found all throughout the Singapore nightlife scene, with the genre constantly evolving with new talented artists and DJs performing regularly. Some favourites include Analog Girl, MYRNE and .gif.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#tunes_2">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/tunes/tunes-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar tunes col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/tunes/img_tunes_urban@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-pink">Urban</h1>
									<h5>Soul Music From The Underground</h5>
									<div className="description">
										<p>Urban music consists generally of hip - hop, R&amp;B, Latin, pop - rap, British R&amp;B, quiet storm, adult contemporary, Latin pop, Chicano R&amp;B, Chicano rap, and Caribbean music such as reggae. Urban music was created through the combination of R&amp;B and soul.</p>
										<p>Urban music was popularised in Singapore in the late 90s and early 2000s. Notable local artists include Urban Exchange, Vandetta, Tim De Cotta, and more recently, new groups and artists like Astronauts, Sam Rui and Mars.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#tunes_3">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/tunes/tunes-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar tunes col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/tunes/img_tunes_pop@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-pink">Pop</h1>
									<h5>Popular With The People</h5>
									<div className="description">
										<p>Pop music is a genre of popular music that has its roots in the U.S. and U.K. in the mid - 1950s. &quot;Pop&quot; and &quot;popular music&quot; are used interchangeably, but the latter includes any music that is popular and includes a multitude of different musical styles. While much of the music that appears on top charts is described as pop, the genre is distinguishable from many songs on the charts.</p>
										<p>Pop music borrows elements from other styles like urban, dance, rock, latin, and country; however, there are core elements that define pop music. Some characteristics include a short to medium length written in a basic format, using repeated choruses, melodic tunes, and hooks.</p>
										<p>Pop's golden era in Singapore was the 1960s to early 1970s, but the genre has seen a revival with local acts garnering strong followings and support from major labels. Acts like The Sam Willows, Sezairi, Charlie Lim, Disco Hue, and Nathan Hartono have all played a part in making Singapore a hotbed of pop music in the region.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="mentor-slide container" data-id="#tunes_4">
						<div className="tunes-overlay-background"><img src="clientlibs/images/timeline/tunes/tunes-overlay-background.jpg" /></div>
						<div className="row">
							<div className="avatar tunes col-12 col-sm-12 col-md-5 col-lg-5"><img className="figure" src="clientlibs/images/timeline/tunes/img_tunes_indie@2x.png" /></div>
							<div className="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div className="scroll-container details">
									<h1 className="title title-pink">Indie</h1>
									<h5>Independent Music</h5>
									<div className="description">
										<p>Indie (independent) music generally refers to music that is produced without the support of a commercial record label or their subsidiaries. While the term &quot;indie&quot; is generally used to describe indie pop and indie rock, the genre includes many musical styles and that cannot be categorised into a single genre. Indie artists also are not confined to one genre and often create music that spans multiple genres.</p>
										<p>Indie music saw a resurgence in Singapore in the late 90s and early 2000s, with acts like Zircon Lounge and Humpback Oak, but continues to sustain a steady fanbase with acts like Subsonic Eye, Sobs and Cosmic Child.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button class="close-overlay">X</button>
			</div>
		</div>
	);
};


// producers
export default TimesTunes;
