// imports
import React, { useEffect } from "react";
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from "isomorphic-unfetch";
import { findWhere } from "underscore";
import { Boombox } from "@components/hidden-boombox";
import { jwtEncodeData } from "@components/jwt-verify";

// component
const SingaporeFinestArtists = ({ bbFlag, bbID, isReal }) => {

	useEffect(() => {
		$('footer').removeClass('relative');
		if(typeof $('.mentors-carousel')[0].slick != 'undefined')	$('.mentors-carousel')[0].slick.refresh();
	}, [0]);

	// render
	return (
		<div class="body-contents">
			<div class="mentors-spotlight"><img src="clientlibs/images/mentors/mentor-spotlight.png" /></div>
			<div class="mentor-landing-background"><img src="clientlibs/images/mentors/body-background.jpg" /></div>
			<div class="container-fluid mentors-landing-page blue">
				<div class="mentors-block">
					<div class="mentor-landing-title">
						<h3>OUR ESTABLISHED ARTISTS</h3>
						<p>Singapore's finest artists who have made their own name in the music industry with their talent and artistry. Get to know them here.</p>
					</div>
					<div class="center-stage-background">
						<div class="background-glow">&nbsp;</div>
					</div>
					<div class="mentors mentors-carousel center">
						<div class="mentor-item">
							<div class="stage"></div><img class="mentor-only" src="clientlibs/images/mentors/mentors-only/img_mentor_joanna_only@2x.png" /><img class="mentor-boombox" src="clientlibs/images/mentors/mentors-boombox/img_mentor_joanna_combined@2x.png" />
							<div class="details">
								<h5 class="title">Joanna Dong</h5>
								<div class="role">Jazz Vocalist, Actress, TV Host</div><a href="#0" overlay-dialog="" id="viewprofile">View profile</a>
							</div>
						</div>
						<div class="mentor-item">
							<div class="stage"></div><img class="mentor-only" src="clientlibs/images/mentors/mentors-only/img_mentor_charlie_only@2x.png" /><img class="mentor-boombox" src="clientlibs/images/mentors/mentors-boombox/img_mentor_charlie_combined@2x.png" />
							<div class="details">
								<h5 class="title">Charlie Lim</h5>
								<div class="role">Singer, Songwriter</div><a href="#1" overlay-dialog="" id="viewprofile">View profile</a>
							</div>
						</div>
						<div class="mentor-item">
							<div class="stage"></div><img class="mentor-only" src="clientlibs/images/mentors/mentors-only/img_mentor_kelly_only@2x.png" /><img class="mentor-boombox" src="clientlibs/images/mentors/mentors-boombox/img_mentor_kelly_combined@2x.png" />
							<div class="details">
								<h5 class="title">Kelly Pan</h5>
								<div class="role">Singer, Songwriter</div><a href="#2" overlay-dialog="" id="viewprofile">View profile</a>
							</div>
						</div>
						<div class="mentor-item">
							<div class="stage"></div><img class="mentor-only" src="clientlibs/images/mentors/mentors-only/img_mentor_sezairi_only@2x.png" /><img class="mentor-boombox" src="clientlibs/images/mentors/mentors-boombox/img_mentor_sezairi_combined@2x.png" />
							<div class="details">
								<h5 class="title">Sezairi</h5>
								<div class="role">Pop &amp; R&amp;B Artist</div><a href="#3" overlay-dialog="" id="viewprofile">View profile</a>
							</div>
						</div>
						<div class="mentor-item">
							<div class="stage"></div><img class="mentor-only" src="clientlibs/images/mentors/mentors-only/img_mentor_shabir_only@2x.png" /><img class="mentor-boombox" src="clientlibs/images/mentors/mentors-boombox/img_mentor_shabir_combined@2x.png" />
							<div class="details">
								<h5 class="title">Shabir</h5>
								<div class="role">Music Artist, Film Composer</div><a href="#4" overlay-dialog="" id="viewprofile">View profile</a>
							</div>
						</div>
					</div>

					{ bbFlag == true && <Boombox className="est-mentor-bb sfa-bb" type={isReal} onClick={() => boomboxClicked()} bbID={bbID} /> }
				</div>
			</div>
			<div class="popover mentor-detail">
				<div class="mentor-container">
					<div class="mentor-slide container" data-id="#0">
						<div class="mentors-overlay-background"><img src="clientlibs/images/mentors/mentor_overlay_bkg.jpg" /></div>
						<div class="row">
							<div class="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img class="figure" src="clientlibs/images/mentors/details/img_mentor_overlay_profileimg_joannadong@2x.png" /></div>
							<div class="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div class="scroll-container details">
									<h1 class="title title-blue">Joanna Dong</h1>
									<h5 class="role">Jazz Vocalist, Actress, TV Host</h5>
									<div class="description">
										<p>Joanna Dong is a jazz vocalist, musical theatre actress and TV host. Over the years, she has established herself as one of the most talented and sought-after jazz vocalists in Singapore, with a repertoire that spans a wide range of audiences. Joanna is bilingual and very passionate about sharing her love for jazz with Mandarin speaking audiences.</p>
										<p>Joanna became the pride of Singapore after a stellar performance at 'Sing! China', coming in as a second runner-up under Jay Chou's team in the popular Chinese singing competition. After her hugely successful two-day '我是真的' (So Here I Am) concert at the Esplanade Concert Hall, she followed up with '我是真的' (So Here I Am) EP digital release and Asia tour across seven different cities.</p>
										<p>Joanna has also performed at other notable events such as Jeremy Monteiro's Jazzy Christmas Concert, Jay Chou's 'The Invincible 2' Concert World Tour, the 2018 National Day Parade, and the 2018 Formula One Singapore Grand Prix.</p>
									</div>
									<ul>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="mentor-slide container" data-id="#1">
						<div class="mentors-overlay-background"><img src="clientlibs/images/mentors/mentor_overlay_bkg.jpg" /></div>
						<div class="row">
							<div class="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img class="figure" src="clientlibs/images/mentors/details/img_mentor_overlay_profileimg_charlie@2x.png" /></div>
							<div class="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div class="scroll-container details">
									<h1 class="title title-blue">Charlie Lim</h1>
									<h5 class="role">Singer, Songwriter</h5>
									<div class="description">
										<p>Charlie Lim is a singer-songwriter and producer based in Singapore. Charlie's debut record 'TIME/SPACE' made it to the number one spot on the Singapore iTunes charts. It was later awarded 'Best Pop Album of the Year' by The Straits Times. His recent album CHECK-HOOK released in 2018 also clinched Best Song of the Year by Apple Music for single Zero-Sum, garnering over 5 million streams to date.</p>
										<p>He played a central role in writing and performing the theme song 'We Are Singapore' for the 2018 National Day Parade. He was also featured on Tatler Gen T Asia 2019 for playing a fundamental role in enriching Singapore's music scene.</p>
										<p>Charlie continues to produce music and contribute to the local scene and is growing to become one of Southeast Asia's best voices in pop music.</p>
									</div>
									<ul>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="mentor-slide container" data-id="#2">
						<div class="mentors-overlay-background"><img src="clientlibs/images/mentors/mentor_overlay_bkg.jpg" /></div>
						<div class="row">
							<div class="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img class="figure" src="clientlibs/images/mentors/details/img_mentor_overlay_profileimg_kellypoon@2x.png" /></div>
							<div class="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div class="scroll-container details">
									<h1 class="title title-blue">Kelly Pan</h1>
									<h5 class="role">Singer, Songwriter</h5>
									<div class="description">
										<p>Kelly became a household name after being crowned a winner in the female category at the inaugural 'Project Superstar Competition', beating over 10,000 applicants.</p>
										<p>Kelly's career has skyrocketed since then, releasing five albums throughout Taiwan, China, Hong Kong, Malaysia and Singapore. At the 19th Singapore Hit Awards, she won the 'Regional Outstanding Artiste Award'. She was honoured with the 'Outstanding Regional Artiste and Vocalist Award' at the 14th Global Chinese Music Awards, before being selected to be the official ambassador for the Global Chinese Music Awards 2015.</p>
										<p>In 2017 she collaborated with Warner Denmark's Christopher on the song 'Heartbeat 心跳', which raked in half a million views in less than one week of its release.</p>
									</div>
									<ul>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="mentor-slide container" data-id="#3">
						<div class="mentors-overlay-background"><img src="clientlibs/images/mentors/mentor_overlay_bkg.jpg" /></div>
						<div class="row">
							<div class="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img class="figure" src="clientlibs/images/mentors/details/img_mentor_overlay_profileimg_sezairi@2x.png" /></div>
							<div class="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div class="scroll-container details">
									<h1 class="title title-blue">Sezairi</h1>
									<h5 class="role">Pop &amp; R&amp;B Artist</h5>
									<div class="description">
										<p>Sezairi is a pop singer whose meteoric rise has left the nation in awe. Sezairi started singing at the tender age of five and hasn't stopped since. He formed a band at the age of 14 and learned the guitar to complement his vocals.</p>
										<p>It was 2009 when Sezairi got his big break, after winning the Singapore Idol series. Since then, he has released hits and chart toppers that remind people of his adeptness in his craft. His recent EP 'Sezairi EP' was completely written by him and co-produced by elite Indonesian producer Ari Renaldi.</p>
										<p>Sezairi creates unique songs that are regularly well received by the public and continues to hone his craft, always looking for ways to make captivating music.</p>
										<p>His achievements include:</p>
									</div>
									<ul>
										<li>Number one spot on Spotify's Viral 50 Chart within a week of the release of his song 'Fire to the Floor'. The song retained the spot for two weeks after.</li>
										<li>Number two spot on Spotify Singapore's Viral 50 Chart within a week of his single release '70s'. This song even made its way to Taiwan, Hong Kong, and the Philippines. The single was also a hit on Indonesia's iTunes and Spotify Viral 50 Chart, while garnering over 400,000 listeners on Spotify.</li>
										<li>Number one on Spotify Singapore and Malaysia Viral 50 Chart within a week of the release of his ballad 'It's You', while it reached number three on Indonesia's iTunes. Two million online listeners tuned in to listen to the song, which has since received a gold certification. The song's music video appeared on the third spot in Apple Music's 'Best of 2018: Editors'</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="mentor-slide container" data-id="#4">
						<div class="mentors-overlay-background"><img src="clientlibs/images/mentors/mentor_overlay_bkg.jpg" /></div>
						<div class="row">
							<div class="avatar col-12 col-sm-12 col-md-5 col-lg-5"><img class="figure" src="clientlibs/images/mentors/details/img_mentor_overlay_profileimg_shabir@2x.png" /></div>
							<div class="about-mentor col-12 col-sm-12 col-md-7 col-lg-7">
								<div class="scroll-container details">
									<h1 class="title title-blue">Shabir</h1>
									<h5 class="role">Music Artist, Film Composer</h5>
									<div class="description">
										<p>Shabir started song writing and song composition when he was 12 years old. Starting out as a self-taught keyboardist, he went on to learn Hindustani classical music under the tutelage of violinist Sri Veereshwar Madhri and guitar from Steve Vatz.</p>
										<p>He is a multiple award-winning singer-songwriter, record producer, music composer and performer. He is the winner of the inaugural singing competition Vasantham Star 2005. He was awarded the Singapore Youth Award, the highest national honour given to young achievers by the Singapore government in 2017. He was the first artist of Tamil and Indian descent to win the award.</p>
										<p>Shabir remains one of the leading recording artists in Singapore with a strong fan following in the region, having released three albums to date 'Alaipayuthey', 'TraffiQ' and 'Swasam-Scents of Prose'.</p>
										<p>Shabir's music is known to be an eclectic concoction of various genres and influences that include pop, alternative rock, hip-hop and world music. Shabir's lyrical style consists of wordplay, metaphor and he often experiments with cadence. His voice culture is characterised by his distinct style that owes itself to his early influences such as Rage Against the Machine, Metallica, Wyclef Jean and Bob Dylan.</p>
										<p>Shabir achieved mainstream success in India as a composer, songwriter and singer in the highly competitive Indian film music industry in 2019 with back-to-back releases, with his songs crossing 70-million collective views on YouTube. To date, he has been signed to eight films as a composer, songwriter and singer, making him the first music artist from Singapore to achieve this feat.</p>
									</div>
									<ul>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button class="close-overlay">X</button>
				<footer class="body-contents">
					<div class="container-fluid">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3">
								<ul class="footer-list footer-left">
									<li>
										<div class="title">Presented by</div>
										<div class="partner-logo"><a class="no-line" href="https://www.temasek.com.sg/" target="_blank"><img src="clientlibs/images/global/logo_temasek_white@2x.png" /></a></div>
									</li>
								</ul>
							</div>
							<div class="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
								<div class="footer-logos-block">
									<div class="footer-logos-content"><img src="clientlibs/images/footer/footer-logo.png" /></div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>
	);
};

// initial props
SingaporeFinestArtists.getInitialProps = async ({ req, query }) => {
	// dynamicaly import producer content

	var request ={
		"page_url": "establish_artist"
	}

	const res = await fetch(process.env.REACT_APP_API_SERVER_URL + 'get-page-boomboxes-list', {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		body: jwtEncodeData(request)
	});

	const response = await res.json();

	let bbFlag	= false;
	let bbID	= 0;
	let isReal	= 'wrong';

	if(response.status == 1) {
		var hasBoombox = findWhere(response.list, {section_id: 'establish_artist'});

		if(typeof hasBoombox != 'undefined') {
			bbFlag	= true;
			bbID	= hasBoombox.boombox_id;
			isReal	= hasBoombox.is_real == 1 ? 'correct' : 'wrong';
		}
	}

	return { bbFlag: bbFlag, bbID: bbID, isReal: isReal };
};

// producers
export default SingaporeFinestArtists;
