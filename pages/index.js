// imports
import React, { useEffect, useRef, useState } from "react";
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from "isomorphic-unfetch";
import Countdown from "@components/countdown";
// component
const HomePage = () => {

	const inputEl = useRef(null);
	const [count, setCount] = useState(false);

	useEffect(() => {
		// add root class
		document.getElementsByTagName("footer")[0].className = "body-contents";
		// slider
		if(typeof $('.home-slider')[0].slick != 'undefined') {
			// refresh
			$('.home-slider')[0].slick.refresh();
			// add class
			$('.home-slider').on('afterChange', function(event, slick, currentSlide){
				document.getElementsByTagName("body")[0].className = 'active-slide_'+ currentSlide;
			});
			$('.home-slider').on('beforeChange', function(event, slick, currentSlide){
				document.getElementsByTagName("body")[0].className = '';
			});
		}
		setTimeout(()=>setCount(true), 500)

	}, [0]);

	// render
	return (
		<React.Fragment>
			<div className="home-slider body-contents">
				<div className="slider-item">
					<div className="home-slider-item">
						<div className="item-content-block">
							<img className="boombox-image for-desktop bg-glow" src={require(`@images/home-page/img_webisodes_combined.png`)}/>
							<img className="boombox-image-mobile for-mobile" src={require(`@images/home-page/img_webisodes_combined_mobile.png`)}/>
							<div className="text-block webisode">
								<div className="text-content">
									<h5>CATCH IT FIRST</h5>
									<h3>LATEST EPISODES</h3>
									<p>Ready for an all-access backstage pass? Enjoy exclusive behind-the-scenes footage of our TGSR artists' music-making journey, and join them as they create original 'Made-in-Singapore' songs. </p>
									<div className="expanding-btn">
										<a href="/media-gallery" id="media-gallery">START WATCHING</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="slider-item">
					<div className="home-slider-item">
						<div className="item-content-block">
							<img className="boombox-image for-desktop showcase" src={require(`@images/home-page/img_tgsr_festival.png`)}/>
							<img className="showcase-image-mobile for-mobile" src={require(`@images/home-page/img_tgsr_festival_mobile.png`)}/>
							<div className="text-block showcase">
								<div className="text-content">
									<h5>ONE NIGHT ONLY </h5>
									<h3>SHOWCASE CONCERT</h3>
									<p>Ready to start your weekend with an unforgettable evening of music and fun? Be one of the first to catch the next generation of emerging music acts as they perform their original 'Made-in-SG' songs live, under the iconic Supertree canopy! This is one incredible evening you do not want to miss!</p>
									<div className="expanding-btn">
										<a href="/showcase-concert" id="gamification">FIND OUT MORE</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="slider-item">
					<div className="home-slider-item">
						<div className="item-content-block mentors-slide">
							<div className="image-block">
								<div className="inner-block">
									<img className="for-desktop" src="clientlibs/images/home-page/carousel/artists-desktop.png" />
									<img className="mentors-mobile for-mobile" src="clientlibs/images/home-page/carousel/artists-mobile.png" />
								</div>
							</div>
							<div className="text-block">
								<div className="text-content">
									<h5>THE BIG REVEAL</h5>
									<h3>MEET THE ASPIRING MUSIC TALENTS</h3>
									<p>230 submissions over 3 weeks. Meet our 10 aspiring music acts who will be participating in Season 2 of The Great Singapore Replay. See who they are here.</p>
									<div className="expanding-btn">
										<a href="/artists" id="artists">MEET THE MUSIC ACTS</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="slider-item">
					<div className="home-slider-item">
						<div className="item-content-block">
							<img className="boombox-image for-desktop" src={require(`@images/home-page/img_prizes_stage.png`)}/>
							<img className="boombox-image-mobile for-mobile" src={require(`@images/home-page/img_prizes_stage_mobile.png`)}/>
							<div className="text-block riffs">
								<div className="text-content">
									<h5>RACK UP RIFFS </h5>
									<h3>WIN AMAZING PRIZES</h3>
									<p>Rack up Riffs by completing a variety of activities and rock your way to the top of the charts for a chance to win amazing prizes! </p>
									<div className="expanding-btn">
										<a href="/gamification" id="gamification">FIND OUT HOW</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

// producers
export default HomePage;
