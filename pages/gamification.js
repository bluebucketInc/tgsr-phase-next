// imports
import React, { useEffect, Fragment } from "react";
import { StickyContainer, Sticky } from 'react-sticky';
import Scrollspy from 'react-scrollspy'
import Slider from "react-slick";

// styles
import "@styles/gamification.scss";

// slider settings
const slickSettings = {
    dots: false,
    infinite: true,
    speed: 600,
    slidesToShow: 3,
    slidesToScroll: 1,
    //centerMode: true,
    centerPadding: '60px',
    className: "prize-carousell",
    lazyLoad: true,
    responsive: [
        {
            breakpoint: 999,
            settings: {
                infinite: false,
                centerPadding: '0px',
                //centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 470,
            settings: {
                infinite: false,
                centerPadding: '0px',
                //centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
            }
        }
    ]
};

// Gamification
const Gamification = ({ prizes, start }) => {
    // add body class
    useEffect(() => {
        document.getElementsByTagName("body")[0].className = "gamification-page-body";

        if(typeof $('.prize-carousell')[0].slick != 'undefined') {
            // refresh
            $('.prize-carousell')[0].slick.refresh();
        }
    }, [0]);

    return (
        <Fragment>
            <div className="container-fluid gamification-container">
                <div className="inner-container">
                    {/* Prize carousel */}
                    <div className="section-container prize">
                        <div className="header">
                            <h3 className="text-uppercase">WIN CHART-TOPPING PRIZES</h3>
                            <p>Get in the groove of racking up Riffs by completing activities and stand to win chart-topping prizes each week. Be the ultimate top 3 Rock Stars and you could win a grand prize!</p>
                        </div>
                        <Slider {...slickSettings} initialSlide={start}>
                            {prizes.map((item, index) => {
                                return (
                                    <div className="slides" key={item.id} >
                                        <div className="content">
                                            <img key={item.id}
                                                src={require(`@images/gamification/${item.img}${item.redeemed ? '-redeem' : ""}.png`)}
                                                alt={item.desc}
                                            />
                                            {item.gp && (
                                                 <span className="grand-prize text-uppercase">{item.gp}</span>
                                            )}
                                           
                                            <p className="desc">{item.desc}</p>
                                            <div className="text-arrow">
                                               {item.id > 5 ? <span>For the period of:</span> : <span>For the week of:</span>} 
                                            </div>
                                            <p className="date">{item.date}</p>
                                        </div>
                                    </div>
                                );
                            })}
                        </Slider>
                    </div>
                </div>
            </div>
            <div className="container gamification-container">
                <div className="inner-container">
                    <StickyContainer>
                        <Sticky topOffset={0}>
                            {
                                ({ style }) => (
                                    <div style={style} className="fixed-nav">
                                        <div className="nav-child-wrapper smooth-scroll">
                                            <Scrollspy items={ ['step1', 'step2', 'step3'] } currentClassName="active" offset={-5}>
                                                <li className="fixed-nav-child">
                                                    <a href="#step1">Begin your journey</a>
                                                </li>
                                                <li className="fixed-nav-child">
                                                    <a href="#step2">Rack up Riffs</a>
                                                </li>
                                                <li className="fixed-nav-child">
                                                    <a href="#step3">Boost your score</a>
                                                </li>
                                            </Scrollspy>
                                        </div>
                                    </div>
                                )
                            }
                        </Sticky>
                        <div className="section-container step-section">
                            <div className="header step-header">
                                <h2 className="text-uppercase align-center">RACKING UP RIFFS IS</h2>
                                <h3 className="text-uppercase align-center">
                                AS EASY AS DO RE MI
                                </h3>
                            </div>
                            <div className="steps">
                                <div className="step-wrapper" id="step1">
                                    <div className="step-content">
                                        <div className="step-title">
                                            <span>1</span>
                                            <p>
                                            Begin your Rock Star journey
                                            </p>
                                        </div>
                                        <p className="step-desc">
                                        Are you ready to rock 'n' roll? Sign in using your Google or Facebook account to begin. 
                                        </p>
                                    </div>
                                    <div>
                                        <img
                                            src={require("@images/gamification/img_point1_graphic.png")}
                                            alt="gamification"
                                        />
                                    </div>
                                </div>
                                <div className="step-wrapper" id="step2">
                                    <div className="first-div">
                                        <img className="for-desktop"
                                            src={require("@images/gamification/img_point2_graphic2.png")}
                                            alt="gamification"
                                            width="100%"
                                        />
                                        <img className="for-mobile graph"
                                            src={require("@images/gamification/img_point2_graphic_mobile.png")}
                                            alt="gamification"
                                        />
                                        
                                    </div>
                                    <div className="step-content second-div">
                                        <div className="step-title">
                                            <span>2</span>
                                            <p>
                                            Collect Riffs to build your fame
                                            </p>
                                        </div>
                                        <p className="step-desc">
                                        Skyrocket your journey to stardom! Scan QR codes at our events and the Showcase Concert, and complete online activities. The more activities you complete, the more Riffs you earn!
                                        </p>
                                    </div>
                                </div>
                                <div className="step-wrapper" id="step3">
                                    <div className="step-content">
                                        <div className="step-title">
                                            <span>3</span>
                                            <p>
                                            Boost your fame with bonuses 
                                            </p>
                                        </div>
                                        <p className="step-desc">
                                        Get ahead of the competition by spotting hidden Bonus Spin Players! Explore the website to find the Bonus Spin Player of the week. Click on it to rack up bonus Riffs! Psst...don't get tricked by the bogus ones!
                                        </p>
                                    </div>
                                    <div>
                                        <img
                                            src={require("@images/gamification/img_point3_graphic.png")}
                                            alt="gamification"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </StickyContainer>
                </div>
            </div>
        </Fragment>
    )
};
// initial props
Gamification.getInitialProps = async ({ req }) => {
    // dynamicaly import prize content
    const res = await fetch(process.env.REACT_APP_API_SERVER_URL + 'get-prizes');
    const response = await res.json();

    const data = response.data;
    return { prizes: data.prizes, start: response.start };
};

// gamification
export default Gamification;
