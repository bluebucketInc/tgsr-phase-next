// imports
import React, { useEffect } from "react";
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from "isomorphic-unfetch";

// styles
import "@styles/artists.scss";

// component
const Artists = ({ artists, mentions }) => {

    // add body class
    useEffect(() => {
        document.getElementsByTagName("body")[0].className = "artists-page-body";
    }, [0]);

    // render
    return (
        <div className="artists-outer">
            <NextSeo title="The Great Singapore Replay: Season 2: The Artists" />
            <div className="container artists-container">
                <div className="inner-container">
                    <div className="page-title text-center">
                        <h2 className="text-uppercase">THE NEXT GENERATION OF</h2>
                        <h3 className="text-uppercase">ASPIRING MUSIC TALENTS</h3>
                        <p className="music-talents">Learn more about our TGSR music talents.</p><p>From their beginnings in music, personalities to their aspirations as artists.</p>
                    </div>
                    <div className="profiles-wrapper">
                        { makeArtists(artists) }
                    </div>
                    {/*<div>
                        <div className="page-title text-center">
                            <h2 className="text-uppercase">eget mauris pharetra et</h2>
                            <h3 className="text-uppercase">Honourable Mentions</h3>
                            <p>
                                Enim tortor at auctor urna nunc id cursus metus
                                aliquam eleifend mi in nulla. Duisque id diam vel
                                quam elementum pulvinar etiam non quam lacus
                                suspendisse faucibus interdum.
                            </p>
                        </div>
                    </div>*/}
                </div>
            </div>
            {/*<div className="mentions-wrapper">
                <div className="honor-row">
                     makeMentions(mentions)
                </div>
            </div>*/}
        </div>
    );
};

// generate artists
const makeArtists = (artists) => {
    // top level items
    let mainItems = [];
    // inner loop
    [1, 2, 3, 4].map(item => {
        // first row
        switch(item) {
            case 1 :  {
                // inner item
                let innerItems = [];
                artists.slice(0, 2).map(i => {
                    innerItems.push(
                        <div className="profile" key={i.name} id={i.id} >
                            <Link href={`/artists/${i.id}`}>
                                <a>
                                    <img src={require(`@images/artists/${i.img.outer}`)} />
                                    <p>{i.name}</p>
                                </a>
                            </Link>
                        </div>
                    )
                });
                // add to main
                mainItems.push(<div key={item} className="two-wrapper inner-wrapper one">{innerItems}</div>)
            }
            break;
            case 2 :  {
                // inner item
                let innerItems = [];
                artists.slice(2, 5).map(i => {
                    innerItems.push(
                        <div className="profile" key={i.name} id={i.id}>
                            <Link href={`/artists/${i.id}`}>
                                <a>
                                    <img src={require(`@images/artists/${i.img.outer}`)} />
                                    <p>{i.name}</p>
                                </a>
                            </Link>
                        </div>
                    )
                });
                // add to main
                mainItems.push(<div key={item} className="three-wrapper inner-wrapper one">{innerItems}</div>)
            }
            break;
            case 3 :  {
                // inner item
                let innerItems = [];
                artists.slice(5, 7).map(i => {
                    innerItems.push(
                        <div className="profile" key={i.name} id={i.id}>
                            <Link href={`/artists/${i.id}`}>
                                <a>
                                    <img src={require(`@images/artists/${i.img.outer}`)} />
                                    <p>{i.name}</p>
                                </a>
                            </Link>
                        </div>
                    )
                });
                // add to main
                mainItems.push(<div key={item} className="two-wrapper inner-wrapper two">{innerItems}</div>)
            }
            break;
            case 4 :  {
                // inner item
                let innerItems = [];
                artists.slice(7, 10).map(i => {
                    innerItems.push(
                        <div className="profile" key={i.name} id={i.id}>
                            <Link href={`/artists/${i.id}`}>
                                <a>
                                    <img src={require(`@images/artists/${i.img.outer}`)} />
                                    <p>{i.name}</p>
                                </a>
                            </Link>
                        </div>
                    )
                });
                // add to main
                mainItems.push(<div key={item} className="three-wrapper inner-wrapper two">{innerItems}</div>)
            }
        }

    })

    return mainItems;
}

// generate honorables
const makeMentions = (mentions) =>
    mentions.map(item => (
        <div className="profile" key={item.image}>
            <div className="img">
                <img src={require(`@images/artists/mentions/${item.image}`)} />
            </div>
            <div className="content">
                <p>{item.name}</p>
                <div className="social">
                    <a href={item.social.fb} target="_blank" className="borderless">
                        <img
                            src={require("@images/logo/facebook.png")}
                        />
                    </a>
                    <a href={item.social.ig} target="_blank" className="borderless">
                        <img
                            src={require("@images/logo/instagram.png")}
                        />
                    </a>
                    <a href={item.social.yt} target="_blank" className="borderless">
                        <img
                            src={require("@images/logo/youtube.png")}
                        />
                    </a>
                </div>
            </div>
        </div>    
    ))

// initial props
Artists.getInitialProps = async ({ req }) => {
    // dynamicaly import producer content
    const data = await import("@content/artists.json");
    return {
        artists: data.default.artists,
        mentions: data.default.mentions,
    };
};

// producers
export default Artists;
