// imports
// import 'react-app-polyfill/ie11';
import React from "react";
import App from "next/app";
import { DefaultSeo } from "next-seo";
import {withRouter} from 'next/router';
import fetch from 'isomorphic-unfetch'
import { toast } from "react-toastify";
import { PointsToast, AlertToast } from "@components/toast";

// components
import DefaultLayout from "@components/_layouts/default";

// global styles
import "@styles/global.scss";

// component
class MyApp extends App {
	constructor({router}){
		super();
		
	}

	render() {
		const { Component, pageProps } = this.props;
		return (
			<DefaultLayout>
				<DefaultSeo
					title="The Great Singapore Replay: Season 2"
					openGraph={{
						type: "website",
						locale: "en_IE",
						url: "https://thegreatsingaporereplay.sg",
						title: 'The Great Singapore Replay: Season 2',
						site_name: "TGSR",
						images: [{
							url: "https://thegreatsingaporereplay.sg/clientlibs/images/global/homepage_og.jpg",
							width: 1200,
							heght: 630,
							alt: "The Great Singapore Replay: Season 2",
						}]
					}}
				/>
				<Component {...pageProps} />
			</DefaultLayout>
		);
	}
}

export default withRouter(MyApp);