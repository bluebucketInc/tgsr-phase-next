// imports
import React from "react";
import FsLightbox from 'fslightbox-react';
import fetch from "isomorphic-unfetch";
import ReactPlayer from "react-player";
import Slider from 'react-slick'
import { pick, map, pluck, findWhere } from "underscore";
import { NextSeo } from "next-seo";
import { toast } from "react-toastify";
import { PointsToast, AlertToast } from "@components/toast";
import { Boombox } from "@components/hidden-boombox";
import { jwtEncodeData } from "@components/jwt-verify";

// components
import { GalleryDescription, ThumbnailWrapper } from "@components/gallery"

// styles
import "@styles/gallery.scss";

// slider settings
const slickSettings = {
	dots: false,
	infinite: true,
	speed: 600,
	slidesToShow: 3,
	slidesToScroll: 1,
	className: "gallery-carousell",
	responsive: [
		{
			breakpoint: 999,
			settings: {
				slidesToShow: 2,
			}
		},
		{
			breakpoint: 470,
			settings: {
				infinite: true,
				slidesToShow: 1,
			}
		},
	]
};

let currentVideoUrl = '';

// gallery
class Gallery extends React.Component {

	// initial props
	static async getInitialProps() {
		// dynamicaly import producer content

		var request ={
			"page_url": "media_gallery"
		}

		const res = await fetch(process.env.REACT_APP_API_SERVER_URL + 'get-page-boomboxes-list', {
			method: 'post',
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: jwtEncodeData(request)
		});

		let bbFlag	= false;
		let bbID	= 0;
		let isReal	= 'wrong';

		const response = await res.json();

		if(response.status == 1) {
			var hasBoombox = findWhere(response.list, {section_id: 'media_gallery'});

			if(typeof hasBoombox != 'undefined') {
				bbFlag = true;
				bbID   = hasBoombox.boombox_id;
				isReal = hasBoombox.is_real == 1 ? 'correct' : 'wrong';
			}
		}

		return { bbFlag	: bbFlag, bbID	: bbID, isReal	: isReal };
	}

	// initial state
	state = {
		masthead: false,
		webisodes: false,
		events: false,
		behindScenes: false,
		webisodeVideos: false,
		isWebisodeActive: false,
		webisodeItem: 1,
	}

	onStart = () => {
		setTimeout(function(){
			albumOrVideoWatched('video', currentVideoUrl);
		}, 30000);
	}

	onReady = (ready) => {
		currentVideoUrl = ready.props.url.replace('https://www.youtube.com/watch?v=', '');
	}

	onStartVideo = () => {
		setTimeout(function(){
			videoWatched('video', currentVideoUrl);
		}, 30000);
	}
	// after mount
	componentDidMount() {

		// add class
		document.getElementsByTagName("body")[0].className = "mediagallery-page-body";

		// // fetch data
		fetch('/clientlibs/gallery/gallery.json')
			.then( r => r.json() )
			.then( data => {
				this.setState({
					masthead: data.main.masthead,
					webisodes: data.webisodes,
					events: data.events,
					behindScenes: data.behindScenes,
					webisodeVideos: pluck(map(data.webisodes, (item) => pick(item, 'video')), 'video'),
				});
			});
	}

	// openslider
	openWebisodeSlider = (item, video) =>{

		this.setState({
			isWebisodeActive: !this.state.isWebisodeActive,
			webisodeItem: item,
		});
		
		currentVideoUrl = video.replace('https://www.youtube.com/watch?v=', '');
		setTimeout(function(){
			onStartVideo('video', currentVideoUrl);
		}, 30000);
	}

	// on event slider
	openDynamicPopup = (event, video) => {
		albumOrVideoWatched('album', event.replace('ev_', '').replace('isActive', ''));

		this.setState({
			[event]: !this.state[event],
		});

		currentVideoUrl = video.replace('https://www.youtube.com/watch?v=', '');
		setTimeout(function(){
			onStartVideo('video', currentVideoUrl);
		}, 30000);
	}
	// render method
	render() {
		return (
			<div className="container gallery-container">
				{
					this.state.masthead &&
						<NextSeo
							title={this.state.masthead.title}
							description={this.state.masthead.desc}
						/>
				}
				<div className="inner-container">

					{/* Mashthead */}
					{
						this.state.masthead &&
							<React.Fragment>
								<div className="header-wrapper">
									<h3 className="header">{this.state.masthead.subtitle}</h3>
									<p>{this.state.masthead.desc}</p>
									<h4 className="section-title">{this.state.masthead.title}</h4>
								</div>
								<div className="masthead-video-wrapper">
									<ReactPlayer
										className="masthead-video"
										url={this.state.masthead.video}
										controls
										playing={false}
										muted
										width="100%"
										height="auto"
										onStart={this.onStart}
										onReady={this.onReady}
									/>
								</div>
							</React.Fragment>
					}

					{/* Webisodes carousel */}
					<div className="section-container webisodes">
						<h4 className="section-title">Webisodes</h4>
						{
							this.state.webisodes &&
							<Slider {...slickSettings} infinite={false} center={false}>
								{
									this.state.webisodes.map(item =>
										<div className="slides" key={item.title}>
											<ThumbnailWrapper
												click={() => this.openWebisodeSlider(item.id,item.video)}
												item={item}
												onClick={this.onStartVideo}
												onReady={this.onReady}
											/>
											<GalleryDescription item={item} />
										</div>
									)
								}
							</Slider>
						}
					</div>

					{/* Behing the scenes carousel */}
					{this.state.behindScenes && (
						<div className="section-container behind-the-scenes">
							<h4 className="section-title">Behind The Scenes</h4>
							<Slider {...slickSettings} infinite={false} center={false}>
								{this.state.behindScenes.map(item => (
									<div className="slides" key={item.title}>
										<ThumbnailWrapper
											click={() =>
												this.openDynamicPopup(
													`bs_${item.id}isActive`, item.video 
												)
											}
											item={item}
										/>
										<GalleryDescription item={item} />
									</div>
								))}
							</Slider>
						</div>
					)}

					{/* Events carousel */}
					<div className="section-container events">
						<h4 className="section-title">Event Photos</h4>
						{
							this.state.events &&
							<Slider {...slickSettings}>
								{
									this.state.events.map(item =>
										<div className="slides" key={item.title}>
											<ThumbnailWrapper
												click={() => this.openDynamicPopup(`ev_${item.id}isActive`, '')}
												item={item}
											/>
											<GalleryDescription item={item} />
										</div>
									)
								}
							</Slider>
						}
					</div>

					{/* Webisode Popup */}
					{
						this.state.webisodeVideos &&
							<FsLightbox
								disableThumbs={true}
								toggler={this.state.isWebisodeActive}
								sources={this.state.webisodeVideos}
								slide={this.state.webisodeItem}
							/>
					}

					{/* Event Popops */}
					{
						this.state.events &&
							this.state.events.map(item =>
								<FsLightbox
									disableThumbs={true}
									key={item.id}
									toggler={this.state[`ev_${item.id}isActive`] || false}
									sources={item.images}
									captions={Array.from({length: item.images.length}, () => <h5>{item.title}</h5>)}
								/>
							)
					}

					{/* Behind the scenes: popup */}
					{
						this.state.behindScenes &&
							this.state.behindScenes.map(item =>
								<FsLightbox
									disableThumbs={true}
									key={item.id}
									toggler={this.state[`bs_${item.id}isActive`] || false}
									sources={item.type === "vid" ? [item.video] : [...item.images]}
									captions={item.type == "img" ? Array.from({length: item.images.length}, () => <h5>{item.title}</h5>) : null}
								/>
							)
					}

					{ this.props.bbFlag == true && <Boombox state="earn" type={this.props.isReal} onClick={() => boomboxClicked()} bbID={this.props.bbID} /> }

				</div>
			</div>
		)
	}
};

function albumOrVideoWatched(watchType, currentTypeID) {

	if(watchType == 'album' || watchType == 'video')
	{
		var request = {
			"event_type"	: watchType == "album" ? "view_album" : "view_video",
			"action_element": currentTypeID
		};

		fetch(process.env.REACT_APP_API_URL + 'add-points', {
			method: 'post',
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: jwtEncodeData(request)
		}).then((res) => {
			res.json().then((response) => {
				if(response.status == 1){
					$('#wriffs').html(parseInt($('#wriffs').html()) + response.points);
					toast(<PointsToast points={response.points} eventMessage={response.event_msg} />);
				}
			});
		});
	}
}

function onStartVideo(watchType, currentTypeID){

	var request = {
		"event_type"	: "view_video",
		"action_element": currentTypeID
	};

	fetch(process.env.REACT_APP_API_URL + 'add-points', {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		body: jwtEncodeData(request)
	}).then((res) => {
		res.json().then((response) => {

			if(response.status == 1){
				$('#wriffs').html(parseInt($('#wriffs').html()) + response.points);
				toast(<PointsToast points={response.points} eventMessage={response.event_msg} />);
			}
		});
	});
}
// producers
export default Gallery;
