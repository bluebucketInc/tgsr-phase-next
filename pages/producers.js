// imports
import React, { useEffect } from "react";
import fetch from "isomorphic-unfetch";
import { NextSeo } from "next-seo";
import { Boombox } from "@components/hidden-boombox";
import { jwtEncodeData } from "@components/jwt-verify";

// components
import SocialContainer from "@components/producer-social-container";

// styles
import "@styles/producers.scss";

// component
const Producers = ({ producers, bbFlagList }) => {
	// add body class
	useEffect(() => {
		document.getElementsByTagName("body")[0].className =
			"producers-page-body";
	}, [0]);

	// render
	return (
		<div className="container producers-container">
			<NextSeo
				title="The Great Singapore Replay: Season 2: The Producers"
				description="Along with Singapore’s finest artists, the aspiring
							young talents will be working with top producers to
							create their songs. These producers, who are well
							versed in a variety of genres, will help in creating
							the one-of-a-kind soundtracks inspired by the
							mashups you voted for."
			/>
			<div className="inner-container">
				<div className="row header-wrapper">
					<div className="col-lg-12 col-md-12">
						<h3 className="header">THE PEOPLE BEHIND THE BEAT</h3>
						<p>
							Along with Singapore’s finest artists, the aspiring
							young talents will be working with top producers to
							create their songs. These producers, who are well
							versed in a variety of genres, will help in creating
							the one-of-a-kind soundtracks inspired by the
							mashups you voted for.
						</p>
						<h2 className="producer-title red-text">Meet the producers</h2>
					</div>
				</div>
				{producers.map((producer, index) => {
					return (
						<div
							className="row producer-container"
							data-aos="fade-up"
							key={producer.name}
						>
							<div className="col-lg-4 col-md-4">
								<img
									className="producer-img"
									src={require(`@images/producers/${
										producer.img
									}`)}
									alt="Rishanda"
								/>
								{
									typeof bbFlagList[producer.name.toLowerCase()] == 'object' && <Boombox type={bbFlagList[producer.name.toLowerCase()]['is_real']} onClick={() => boomboxClicked()} bbID={bbFlagList[producer.name.toLowerCase()]['boombox_id']} />
								}
							</div>
							<div className="col-lg-8 col-md-8">
								<div className="producer-name">
									<h4>{producer.name}</h4>
									<SocialContainer
										fb={producer.fb}
										ig={producer.ig}
										yt={producer.yt}
									/>
								</div>
								{producer.desc.map(p => (
									<p key={p}>{p}</p>
								))}
							</div>
						</div>
					);
				})}
			</div>
		</div>
	);
};

// initial props
Producers.getInitialProps = async ({ req }) => {
	// dynamicaly import producer content

	var request ={
		"page_url": "producer"
	}

	const res = await fetch(process.env.REACT_APP_API_SERVER_URL + 'get-page-boomboxes-list', {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		body: jwtEncodeData(request)
	});

	const response = await res.json();
	let bbFlagList = {};
	if(response.status == 1) {

		for (var i = response.list.length - 1; i >= 0; i--) {
			bbFlagList[response.list[i]['section_id'].toLowerCase()] = {
				'boombox_id': response.list[i]['boombox_id'],
				'is_real'	: (response.list[i]['is_real'] == 1 ? 'correct' : 'wrong')
			};
		}
	}

	const data = await import("@content/producers.json");
	return { producers: data.default.producers, bbFlagList: bbFlagList };
};

// producers
export default Producers;
