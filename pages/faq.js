// imports
import React, { useEffect } from "react";
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from "isomorphic-unfetch";

// component
const Faq = () => {

	useEffect(() => {
		// add root class
		$('footer').removeClass('relative');
	})

	// render
	return (
		<div class="faq-page body-contents">
			<div class="background-photo">
				<img src="clientlibs/images/lights/faq-background.jpg" />
			</div>
			<div class="background-pattern">
			</div>
			<div class="container-2">
				<h3>FREQUENTLY ASKED QUESTIONS</h3>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-4 col-lg-4">
						<div class="mobile-dropdown">
							<div class="dropdown">
								<span>About The Great Singapore Replay: Season 2</span>
								<i class="fa fa-angle-down">
								</i>
								<i class="fa fa-angle-up">
								</i>
							</div>
							<ul class="faq-list">
								<li>
									<a class="active" href="#accordion1">About The Great Singapore Replay: Season 2</a>
								</li>
								<li>
									<a href="#accordion3">The 5 Iconic Eras And 5 Modern Genres</a>
								</li>
								<li>
									<a href="#accordion4">About the Mashup</a>
								</li>
								<li>
									<a href="#accordion2">Selected Mashups And <br/>Music Acts</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-8 col-lg-8">
						<div class="accordion component md-accordion projectList list" role="tablist" aria-multiselectable="true" id="accordion1">
							<div class="card">
								<div class="card-header" id="accordion-bar-1" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
										<div class="title">What is The Great Singapore Replay (TGSR)?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse show" id="collapse1" role="tabpanel" aria-labelledby="accordion-bar-1" data-parent="#accordion1">
									<div class="card-body">
										<p>Presented by Temasek, the biennial TGSR is about discovery and collaboration - discovery of Singapore's rich music heritage and the collaboration between established artists and the next generation of aspiring young local talents to create original 'Made-in-Singapore' songs.</p>
										<p>In its second season, TGSR offers 10 music talents the opportunity to be guided by five established artists in creating 10 new original songs. These songs will be inspired by mashups of distinct tunes, voted by the public, from five iconic eras of Singapore's history and five modern music genres.</p>
										<p>Discovery and collaboration have always been the heart and soul of TGSR, and this season will have no shortage of either. Expect to see an even bigger collaboration of artists and industry professionals, in this continued effort to nurture Singapore's music scene and showcase our diversity of aspiring young talents.</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="accordion-bar-2" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
										<div class="title">Who are the people behind TGSR?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse" id="collapse2" role="tabpanel" aria-labelledby="accordion-bar-2" data-parent="#accordion1">
									<div class="card-body">
										<p>TGSR is presented by Temasek, with the support of music labels - Sony Music, Universal Music Group and Warner Music.</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="accordion-bar-3" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
										<div class="title">How is this different from other music mentorship programmes</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse" id="collapse3" role="tabpanel" aria-labelledby="accordion-bar-3" data-parent="#accordion1">
									<div class="card-body">
										<p>TGSR is not just any mentorship programme. It is an enabler of aspiring local music talents, a collaborative platform for the Singapore music industry, and a meaningful touchpoint to bring the community together.</p>
										<p>Through TGSR, we aim to nurture local music talent by helping them to work with established industry professionals and provide them a platform to showcase their talent to the world.</p>
										<p>Working with local music industry players, the selected talents will undergo a professional production experience, comprising song-writing workshop, song arrangement and recording. It will also include the marketing and creating of individual artists' brand, and the launch of their songs on popular digital music streaming platforms.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="accordion component md-accordion projectList list" role="tablist" aria-multiselectable="true" id="accordion2">
							<div class="card">
								<div class="card-header" id="accordion-bar-4" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
										<div class="title">How were the final 10 mashups selected?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse show" id="collapse4" role="tabpanel" aria-labelledby="accordion-bar-4" data-parent="#accordion2">
									<div class="card-body">
										<p>The final 10 mashups were selected by the public through voting and our established artists based on the potential for musicality and creative expression in the creation of the 10 original "Made-in-Singapore" songs.</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="accordion-bar-5" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
										<div class="title">How were the 10 emerging music acts selected?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse" id="collapse5" role="tabpanel" aria-labelledby="accordion-bar-5" data-parent="#accordion2">
									<div class="card-body">
										<p>The final 10 acts were selected by industry experts from Sony Music and Warrior records basis the following 4 judging criteria:</p>
										<ul>
											<li>Talent &amp; Musicality</li>
											<li>Character &amp; Personality</li>
											<li>Growth Potential</li>
											<li>Depth of Material</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="accordion component md-accordion projectList list" role="tablist" aria-multiselectable="true" id="accordion3">
							<div class="card">
								<div class="card-header" id="accordion-bar-6" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
										<div class="title">What are the eras? How were they chosen?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse show" id="collapse6" role="tabpanel" aria-labelledby="accordion-bar-6" data-parent="#accordion3">
									<div class="card-body">
										<p>These are the 5 iconic eras:</p>
										<ul>
											<li>1300s - An island called Temasek</li>
											<li>1400 - 1700 - The rise of Singapura in Southeast Asia</li>
											<li>1819 - Arrival of the British</li>
											<li>1900 - 1945 - Pre-War Singapore</li>
											<li>1965 - Present - Journey into Nationhood</li>
										</ul>
										<p>The eras were chosen to depict the milestone moments of history that shaped our culture and identity.</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="accordion-bar-7" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
										<div class="title">What are the genres? How were they chosen?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse" id="collapse7" role="tabpanel" aria-labelledby="accordion-bar-7" data-parent="#accordion3">
									<div class="card-body">
										<p>These are the 5 modern genres:</p>
										<ul>
											<li>Jazz - Roots Music Of The world</li>
											<li>Pop - Popular With People</li>
											<li>Indie - Independent Music</li>
											<li>Electronic - Music Of The Technological age</li>
											<li>Urban - Soul Music From The Underground</li>
										</ul>
										<p>These genres of music were chosen to represent a comprehensive cross-section of the genres in which music is created and listened to today. They cover (though not exhaustively) a wide proportion of how all music can be classified in terms of style and approach.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="accordion component md-accordion projectList list" role="tablist" aria-multiselectable="true" id="accordion4">
							<div class="card">
								<div class="card-header" id="accordion-bar-8" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse8" aria-expanded="true" aria-controls="collapse8">
										<div class="title">What is a mashup?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse show" id="collapse8" role="tabpanel" aria-labelledby="accordion-bar-8" data-parent="#accordion4">
									<div class="card-body">
										<p>A mashup is a unique, distinct tune created by combining the sounds of one of the 5 iconic eras of Singapore's history and one of the 5 modern music genres. </p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="accordion-bar-9" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse9" aria-expanded="true" aria-controls="collapse9">
										<div class="title">Why should I create a mashup?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse" id="collapse9" role="tabpanel" aria-labelledby="accordion-bar-9" data-parent="#accordion4">
									<div class="card-body">
										<p>Out of 25 unique combinations, the 10 most popular mashups, voted by the public and established artists, will inspire the creation of 10 original "Made-in-Singapore" songs by 10 aspiring TGSR music talents.</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="accordion-bar-10" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse10" aria-expanded="true" aria-controls="collapse10">
										<div class="title">How can I create my own mashup?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse" id="collapse10" role="tabpanel" aria-labelledby="accordion-bar-10" data-parent="#accordion4">
									<div class="card-body">
										<p>On the Spin Player, pick an iconic era and a modern music genre, and then hit the play button to listen to your mashup. Remember to turn on the volume!</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="accordion-bar-11" role="tab">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapse11" aria-expanded="true" aria-controls="collapse11">
										<div class="title">Where can I find out more about my mashup?</div>
										<i class="fa fa-angle-down">
										</i>
										<i class="fa fa-angle-up">
										</i>
									</a>
								</div>
								<div class="collapse" id="collapse11" role="tabpanel" aria-labelledby="accordion-bar-11" data-parent="#accordion4">
									<div class="card-body">
										<p>You can learn more about the your mashup tune by clicking on the ticker tape above the play button on the spin player.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};


// producers
export default Faq;
