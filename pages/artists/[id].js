// imports
import React, { useEffect } from "react";
import Link from "next/link";
import Router from 'next/router'
import { NextSeo } from "next-seo";
import { findWhere } from "underscore";
import { Boombox } from "@components/hidden-boombox";
import { jwtEncodeData } from "@components/jwt-verify";

// styles
import "@styles/artists-detail.scss";

// component
const ArtistDetail = ({ artist, bbFlag, bbID, isReal }) => {



	// add body class
	useEffect(() => {
		document.getElementsByTagName("body")[0].className =
			"artist-details-page-body";


			// console.log(Router, Router.back);
	}, [0]);

	// render
	return (
		<React.Fragment>
			<div className="container artist-page-container">
				<NextSeo
					title={`The Great Singapore Replay: Season 2: ${artist.name}`}
					description={artist.desc[0]}
				/>
				<div className="row">
					<div className="col-lg-6 col-md-12 artist-img">
						<img
							className="artist-image d-none d-sm-block"
							src={require(`@images/artists/details/${
								artist.img.inner
							}`)}
						/>
						<img
							className="artist-image d-block d-sm-none"
							src={require(`@images/artists/details/mobile/${
								artist.img.inner
							}`)}
						/>
					</div>
					<div className="col-lg-6 col-md-12 artist-content">
						<h1>{artist.name}</h1>
						<div className="internal-links">
							<p>
								<span>Mashup: </span>
								<a href={`/mashup-gallery${artist.mashup.link}`}>{artist.mashup.name}</a>
								{ false ?
								<Link href={`/mashup-gallery/${artist.mashup.link}`}>
									<a>{artist.mashup.name}</a>
								</Link>
								: ''}
							</p>
							<span className="separator">|</span>
							<p>
								<span>Mentor: </span>
								<a href={artist.mentor.link}>{artist.mentor.name}</a>
								{ false ?
								<Link href={artist.mentor.link}>
									<a>{artist.mentor.name}</a>
								</Link>
								: ''}
							</p>
						</div>
						<div className="social">
							{artist.social.fb && (
								<a
									href={artist.social.fb}
									target="_blank"
									className="borderless"
								>
									<img
										src={require("@images/logo/facebook.png")}
									/>
								</a>
							)}
							{artist.social.ig && (
								<a
									href={artist.social.ig}
									target="_blank"
									className="borderless"
								>
									<img
										src={require("@images/logo/instagram.png")}
									/>
								</a>
							)}
							{artist.social.yt && (
								<a
									href={artist.social.yt}
									target="_blank"
									className="borderless"
								>
									<img
										src={require("@images/logo/youtube.png")}
									/>
								</a>
							)}
							{artist.social.sp && (
								<a
									href={artist.social.sp}
									target="_blank"
									className="borderless"
								>
									<img
										src={require("@images/logo/spotify.png")}
									/>
								</a>
							)}
						</div>
						<div className="inner-content">
							{artist.desc.map(item => (
								<p key={item}>{item}</p>
							))}

							{ bbFlag == true && <Boombox type={isReal} onClick={() => boomboxClicked()} bbID={bbID} /> }
						</div>
					</div>
				</div>
			</div>
			<a className="close-btn" onClick={() =>
				(document.referrer.includes("thegreatsingaporereplay") || document.referrer.includes("tgsr")) ? Router.back() : Router.push('/artists')}>
				<img
					src={require("@images/logo/popup-close.png")}
					alt="Close"
				/>
			</a>
		</React.Fragment>
	);
};

// initial props
ArtistDetail.getInitialProps = async ({ req, query }) => {
	// dynamicaly import producer content

	var request ={
		"page_url": "artist"
	}

	const res = await fetch(process.env.REACT_APP_API_SERVER_URL + 'get-page-boomboxes-list', {
		method: 'post',
		headers: {
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		body: jwtEncodeData(request)
	});

	const response = await res.json();

	let bbFlag	= false;
	let bbID	= 0;
	let isReal	= 'wrong';

	if(response.status == 1) {
		var hasBoombox = findWhere(response.list, {section_id: query.id});

		if(typeof hasBoombox != 'undefined') {
			bbFlag = true;
			bbID   = hasBoombox.boombox_id;
			isReal = hasBoombox.is_real == 1 ? 'correct' : 'wrong';
		}
	}

	const data = await import("@content/artists.json");
	return {
		artist	: findWhere(data.default.artists, {id: query.id}),
		bbFlag	: bbFlag,
		bbID	: bbID,
		isReal	: isReal,
	};
};

// producers
export default ArtistDetail;
