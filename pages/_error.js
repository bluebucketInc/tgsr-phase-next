// imports
import React, { useEffect } from 'react'
import { Button } from "@components/button";

function Error({ statusCode }) {

	var btnCss = {
		'fontSize': 14,
	};

	var divCss = {
		'margin': '0 auto'
	};

	useEffect(() => {
		document.getElementsByTagName("body")[0].className = "qr-page-body";
	}, [0]);

	return (
		<div className="container qr-container">
			<div className="inner-container">
				<div className="row header-wrapper">
					<div className="col-lg-12 col-md-12">
						<h3 className="header">
							OOPS..
							<br />
							THERE IS AN ERROR.
						</h3>
						<p>Something went wrong. Please try again later.</p>
						<div className="bottom-wrapper">
							<div className="expanding-btn text-center" style={divCss} >
								<a href="/" style={btnCss} >BACK TO HOME</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

Error.getInitialProps = ({ res, err }) => {
	const statusCode = res ? res.statusCode : err ? err.statusCode : 404
	return { statusCode }
}

export default Error