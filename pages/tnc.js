// imports
import React, { useEffect } from "react";
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from "isomorphic-unfetch";

// component
const Tnc = () => {

	var customStyle = {
		'marginTop': 15
	}
	var customStyle2 = {
		'marginBottom': 20
	}

	// add body class
	useEffect(() => {
		document.getElementsByTagName("body")[0].className = "about-background other-pages";
	}, [0]);

	// render
	return (
		<div class="about-page body-contents tnc-page">
			<div class="container-2">
				<div class="page-title other-title" data-aos="fade-up">
					<h5>THE GREAT SINGAPORE REPLAY </h5>
					<h3>WEBSITE TERMS OF USE</h3><br /><br />
					<p>Please read these terms of use carefully before using this Website. In these terms, "Website" refers to our website, including any information, services, contents, functionalities, (including Create Your Own Mash-Up segment of our website), products or features that form part of such website.</p>
				</div>
				<h5>PART A - GENERAL TERMS TO THIS WEBSITE</h5>
				<div class="order-sublist">
					<ol class="main-list">
						<li data-aos="fade-up">
							<h5 class="title-blue">GENERAL</h5>
							<ol class="sublist">
								<li data-aos="fade-up">
									<p>All access of any part of <a href = "https://thegreatsingaporereplay.sg/">https://thegreatsingaporereplay.sg/</a> (“Website”) is governed by the terms below. In these terms, the words “Temasek”, “we”, “our” and “us” refer to Temasek Capital Management Pte. Ltd; and “you”, “your” and the “user” refers to any person and/or entity accessing and/or using the Website.</p>
								</li>
								<li data-aos="fade-up">
									<p>
									By browsing, accessing and/or using the Website, you signify you have read and accept these terms of use. If you do not agree to these terms of use, please exit our Website, and refrain from any further use and/or access to our Website. The user is responsible for compliance with all applicable laws and regulations in any jurisdiction.</p>
								</li>
								<li data-aos="fade-up">
									<p>We may amend these terms from time to time. Amendments shall take effect from the date the amended terms are published at <a href="https://thegreatsingaporereplay.sg/">https://thegreatsingaporereplay.sg/</a>. If you use or access our Website after such amendments, you agree to the amended terms and to be bound by them. It is your responsibility to check for the latest version of the terms.</p>
									</li>
									<li data-aos="fade-up">
										<p>
										We may from time to time publish additional guidelines, rules, and conditions applicable to your use of our Website. You agree to comply with these additional guidelines, rules and conditions, which are incorporated by reference into these terms.</p>
									</li>
									<li data-aos="fade-up">
										<p>
										These terms only govern our provision and your use of our Website. Certain transactions and activities, including the provision of services or products may be performed or facilitated through our Website (e.g. Open Call) may be subject to separate terms and conditions that you have to accept in order to enter into these transactions or perform these activities.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">NO OBLIGATION TO SUPPORT OR MAINTAIN</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>We have no obligation to provide, or continue to provide our Website, or any part thereof, now or in the future. We reserve the right, at any time, temporarily or permanently, in whole or in part, without prior notification and without incurring any liability to you, to: modify, suspend or discontinue our Platforms; charge for the use of our Website; restrict or modify access to our Website; and modify and/or waive any charges in connection with our Website.</p>
									</li>
									<li data-aos="fade-up">
										<p>We have no obligation to provide any maintenance, support or other services in relation to the Website, including providing any telephone assistance, documentation, error corrections, updates, upgrades, bug fixes, patches, and/or enhancements.</p>
									</li>
									<li data-aos="fade-up">
										<p>If and when we provide maintenance, support or other services in relation to our Website, your access and use of our Website may be interrupted, suspended or restricted.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">RESTRICTIONS ON USE OF MATERIALS</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>The Website (including, for the avoidance of doubt, all materials located therein, logos, trade marks, service marks, domain names, trade names, designs, contents made available through our Website (including pages, documents and online graphics, audio and video), the source and object codes, and the format, directories, queries, algorithms, structure and organisation of our Website), are proprietary to us, and all intellectual property rights associated therewith, whether registered or not, are protected by law and owned by or licensed to us.</p>
									</li>
									<li data-aos="fade-up">
										<p>References to any names, marks, products or services of third parties do not necessarily constitute or imply Temasek's endorsement, sponsorship or recommendation of the third party, information, product or service. To the maximum extent permitted by law, we are not responsible and disclaim all liability for (A) such third party products, services, websites; (B) for any act or omission of these third parties, or (C) any dealings between you and these third parties, whether or not such dealings have been performed or facilitated through our website.</p>
									</li>
									<li data-aos="fade-up">
										<p>No material, graphic or image from the Website may be appropriated or modified in any manner, or reproduced, republished, uploaded, posted, transmitted or distributed in any way, without the prior written permission of Temasek.</p>
									</li>
									<li data-aos="fade-up">
										<p>No party is permitted to establish links to the Website without prior written permission from Temasek. Temasek reserves all right to deny permission for any such links. Temasek is not under any obligation to establish reciprocal links with any third party. Nothing contained herein confers any license or right under any copyright, patent, trademark or other proprietary rights of Temasek or any third party.</p>
									</li>
									<li data-aos="fade-up">
										<p>Neither Temasek, any of its affiliates, subsidiaries, employees, agents, partners, principals and representatives nor any other person is, in connection with this site, engaged in rendering auditing, accounting, tax, legal, advisory, consulting or other professional services or advice. Each user agrees not to use the content of the Website as a substitute for independent investigations and competent judgment of the user of the Website and such user shall obtain professional advice tailored to his particular factual situation. Each user is responsible for all matters arising from its use of the Website.</p>
									</li>
									<li data-aos="fade-up">
										<p>You agree to use the Website in accordance with these terms of use and for lawful and proper purposes and shall not:-</p>
										<ol>
											<li data-aos="fade-up">
												<p> </p>modify, adapt, improve, enhance, alter, translate or create derivative works of our Website;
											</li>
											<li data-aos="fade-up">
												<p></p>use or merge our Website, or any component or element thereof, with other software, databases or services not provided by our Website;
											</li>
											<li data-aos="fade-up">
												<p></p>reverse engineer, decompile, disassemble or otherwise attempt to derive the source code or structure of our Website, or decrypt our Website;
											</li>
											<li data-aos="fade-up">
												<p></p>interfere in any manner with the operation of our Website;
											</li>
											<li data-aos="fade-up">
												<p></p>circumvent, or attempt to circumvent, any electronic protection measures in place to regulate or control access to our Website;
											</li>
											<li data-aos="fade-up">
												<p></p>create a database by systematically downloading and storing our Website;
											</li>
											<li data-aos="fade-up">
												<p></p>use any robot, spider, site search/retrieval application or other manual or automatic device to retrieve, index, “scrape”, “data mine”, “crawl” or in any way gather our Website or reproduce or circumvent the navigational structure or presentation of our Website;
											</li>
											<li data-aos="fade-up">
												<p></p>upload, post, email, transmit or otherwise make available any content that is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libellous, invasive of another's privacy, hateful, or racially, ethnically or otherwise objectionable or which creates liability on Temasek’s part;
											</li>
											<li data-aos="fade-up">
												<p></p>upload, post, email, transmit or otherwise make available any unsolicited or unauthorized advertising, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of solicitation, except in those areas (such as shopping rooms) that are designated for such purpose;
											</li>
											<li data-aos="fade-up"> 
												<p></p>upload, post, email, transmit or otherwise make available any material that contains software viruses or any other computer code, files or programmes designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; or
											</li>
											<li data-aos="fade-up">
												<p></p>interfere or attempt to interfere with the operation or functionality of the Website; or obtain or attempt to obtain unauthorized access, via whatever means, to any of Temasek’s systems.
											</li>
										</ol>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">THIRD PARTY POSTINGS AND HYPERLINKS TO THIRD PARTY WEBSITES</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>The Website may contain hyperlinks to other websites ("External Sites") which are neither maintained nor controlled by Temasek, or may contain content posted on or via the Website by third parties. Temasek shall not be responsible for any errors or omissions in any content in the Website, or the content, products or services of any hyperlinked External Site or any hyperlink contained in a hyperlinked External Site, nor for the privacy and security practices employed by these External Sites, and under no circumstances shall Temasek be liable for any loss or damage of any kind incurred as a result of the use of any content posted or contained in e-mails or otherwise transmitted or displayed via the Website, or arising from access to External Sites. Use of the Website and any hyperlinks and access to External Sites are entirely at the user’s own risk.</p>
									</li>
									<li data-aos="fade-up">
										<p>The user acknowledges that Temasek has no control over and excludes all liability for any material on the Internet which can be accessed by using the Website. Neither will Temasek be deemed to have endorsed any such content thereto.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">COOKIES</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>Temasek takes certain industry-accepted precautions to secure the Website or portions thereof. However, the user understands and agrees that such precautions cannot guarantee that use of the Website is invulnerable to security breaches, nor does Temasek make any warranty, guarantee, or representation that use of the Website is protected from all viruses, worms, Trojan horses, and other vulnerabilities.</p>
									</li>
									<li data-aos="fade-up">
										<p>During your use of the Website, Temasek may issue to and request from the user’s computer "cookies" to enable more convenient browsing when the user revisits the Website. The user must not alter any cookies sent to his/her computer from the Website and must ensure that the user’s computer sends correct and accurate cookies in response to any relevant request from the Website.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">PERSONAL DATA</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>Temasek is committed to protecting and respecting your personal data.</p>
									</li>
									<li data-aos="fade-up">
										<p>Your personal data may be collected by us when you access and use our Website. You consent to our collection, use, disclosure and processing of your personal data in accordance with the <a href="/privacy.html">TGSR Privacy & Cookie Notice</a>.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">NO REPRESENTATION OR WARRANTIES</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>While every effort has been made to ensure that the information provided in this website herein is up to date and accurate, Temasek and its affiliates, subsidiaries, employees, agents, partners, principals and representatives make no representation or warranty of any kind either express or implied as to the completeness, correctness, accuracy, suitability, reliability or otherwise of the website or its content in this website or the results of its use for any purpose, all of which is provided "as is" and "as available". They do not warrant that this website or this server that makes it available is free of any virus or other harmful elements.</p>
									</li>
									<li data-aos="fade-up">
										<p>Temasek and its affiliates, subsidiaries, employees, agents, partners, principals and representatives disclaim all warranties and obligations relating to this website, including but not limited to all implied warranties and obligations of merchantability, fitness for a particular purpose, title and non-infringement.</p>
									</li>
									<li data-aos="fade-up">
										<p>Your access and use of our Website is voluntary and at your sole risk. You are solely responsible for: (a) your reliance on our Website and its contents; (b) any liability or damage that you may incur through use of our Website and its contents; and (c) for all decisions or actions resulting from your access and use of our Website and its contents.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">LIMITATION OF LIABILITY</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>The information contained herein in this website shall be accessed and used at the user's own risk and to the fullest extent permissible and subject and pursuant to all applicable laws and regulations. Temasek and its affiliates, subsidiaries, employees, agents, partners, principals and representatives shall not be liable for any direct, indirect, incidental, special, exemplary, consequential or other damages whatsoever, (including but not limited to liability for loss of use, data or profits), including but not limited to contract, negligence or other tortious actions, arising out of or in connection with this site, even if any of Temasek and its affiliates, subsidiaries, employees, agents, partners, principals and representatives has been advised of the possibility of such damages or losses that were, are being or will be incurred.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">USER ACKNOWLEDGEMENTS</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>The user acknowledges and agrees that for our Website to function, it may require a compatible device (including a mobile or computing device), appropriate third party software (such as browsers), and also connectivity to the internet. You are solely responsible for obtaining such device(s), software, and the necessary connectivity services to access and use our Website. We assume no responsibility for such devices, software and services, or for any functionality of our Website which are dependent on them to operate.</p>
									</li>
									<li data-aos="fade-up">
										<p>You will indemnify and at all times keep us and our related corporations and affiliates (including respective officers, directors, employees and agents) indemnified against any and all losses, damages, actions, proceedings, costs, expenses, claims, demands, liabilities (including full legal costs) which may be suffered or incurred by us or asserted against us by any person, party or entity whatsoever, in respect of any matter or event whatsoever arising out of or in connection with your breach of any provision in these terms, your access and use of our Website, or your violation of the rights of any third party.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">PARTICIPATION IN ANY CONTESTS AND/OR PROMOTIONAL ACTIVITIES ON OUR WEBSITE</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>Notwithstanding Clause 1.5 above, all employees (including their immediate family members) of (i) Temasek; (ii) Temasek’s affiliates, partners and/or vendors involved in The great Singapore Replay (“TGSR”); and (iii) such persons as Temasek deems in its sole discretion to be involved in TGSR, are not eligible to participate in any contests, marketing campaign and/or promotional activities that may be offered or made available on the Website.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">MODIFICATIONS</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>Temasek reserves all rights to make any and all changes in this website at its sole discretion without prior notice to the user. Temasek reserves the right to deny access to this website to anyone at any time without prior notice. Materials on our website may include technical inaccuracies or typographical errors. Changes may be periodically incorporated into this material. Temasek may make improvements and/or changes in the products, services and/or programmes described in these materials at any time without notice.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">SUGGESTIONS, COMMENTS AND FEEDBACK</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>Should the user respond to any part of the materials contained herein in this website with any communications including feedback data, such as questions, comments, suggestions, or the like, such information shall be deemed to be non-confidential and Temasek and its affiliates, subsidiaries, employees, agents, partners, principals and representatives shall have no obligation whatsoever with respect to such communications and shall be free to reproduce, use disclose and distribute the information to others without limitation, and shall be free to use in any way for any purpose whatsoever the content of such communications including any ideas, know-how, techniques or concepts disclosed therein.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">REPORTING VIOLATIONS</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>If you discover any content on our Website that you believe infringes your intellectual property rights or which is otherwise illegal, offensive, defamatory, obscene, you may report such content to us at <a href="mailto:tgsr@temasek.com.sg?Subject=Reporting%20Violations">tgsr@temasek.com.sg</a></p>
									</li>
									<li data-aos="fade-up">
										<p>Your report should contain at least the following information: (a) clear identification of the specific content complained of, including details of where such content can be located; (b) where applicable, clear description of your intellectual property or any other right or law that you claim has been infringed; (c) reasons to support your belief that the content infringes your intellectual property rights or is otherwise illegal, offensive, defamatory, obscene; and (d) your contact details including your name, address, telephone number, email address and your user name (if applicable). We may request for such additional information or documents as we deem fit to evaluate your report. You warrant and represent that all information provided in connection with such report shall be true, accurate and complete.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">APPLICABLE LAW AND JURISDICTION</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>The law applicable to the terms of use of this website is the law of the Republic of Singapore and the courts of the Republic of Singapore will have exclusive jurisdiction in case of any dispute. Any claim you may have against us in connection with these terms or your access and use of our Website must be commenced within one (1) year from the claim arising.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">MISCELLANEOUS</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>Our Website is controlled and operated by us from Singapore, and is not intended to subject us to the laws or jurisdiction of any state, country or territory other than that of Singapore. We do not represent or warrant that our Website is appropriate or available for use in any particular jurisdiction other than Singapore.</p>
									</li>
									<li data-aos="fade-up">
										<p>If any provision of these terms is found to be unlawful, void or for any reason unenforceable, that provision will be deemed severable from these terms and will not affect the validity and enforceability of the remaining provisions. These terms constitute the entire agreement between you and us relating to the subject matter herein and supersede any and all prior or contemporaneous written or oral representations (including information found on our Platforms). Our failure to insist upon or enforce strict performance of any provision of these terms shall not be construed as a waiver of any provision or right. A person who is not a party to these terms shall have no right under the Contracts (Rights of Third Parties) Act (Cap. 53B) to enforce these terms. You may not assign your rights or obligations under these terms. We may assign our rights or obligations under these terms to any third party at any time without your consent or notice to you.</p>
									</li>
								</ol>
							</li>
							<h5>PART B - ADDITIONAL TERMS IN RELATION TO WEEKLY MASHUP VOTING</h5>
							<li data-aos="fade-up">
								<h5 class="title-blue">WEEKLY MASHUP VOTING</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>In addition to the terms in Part A above, the terms in this Part B (collectively, “Campaign Terms”) applies to you if you participate in our Weekly Mashup Voting (“Campaign”), and you have been notified that you have won a prize in the Campaign.</p>
									</li>
									<li data-aos="fade-up">
										<p>The Prizes are not replaceable (whether in whole or in part), exchangeable or redeemable for cash or in-kind.
										</p>
									</li>
									<li data-aos="fade-up">
										<p>The images of Prizes on all marketing materials are for illustration purposes only. Actual products may be different from the images shown. Prizes cannot be assigned, transferred, redeemed for cash or substituted. We will not replace any lost or stolen Prizes.</p>
									</li>
									<li data-aos="fade-up">
										<p>We may, in our sole and absolute discretion and without prior notice, substitute, withdraw, add to or alter any of the Prizes offered with another of comparable value if the Prize is unavailable.</p>
									</li>
									<li data-aos="fade-up">
										<p>The value of the Prize shall be solely determined by us.</p>
									</li>
									<li data-aos="fade-up">
										<p>The Prizes may be subject to the terms and conditions of the respective third party manufacturer, supplier and/or distributor (if any). We shall not be liable for any separate terms and conditions as may be imposed by such third parties for the use of the Prizes.</p>
									</li>
									<li data-aos="fade-up">
										<p>Taxes, if any, related to a Prize are the sole responsibility of the Prize winner. We accept no responsibility for any tax or other liability that may arise from the Prize winnings.</p>
									</li>
									<li data-aos="fade-up">
										<p>The Prizes are provided on “as is”, “with all faults” and “as available” basis. We do not warrant, represent or guarantee the usability or quality of the Prizes that the claims made by the manufacturers, suppliers and/or distributors of those Prizes are accurate. To the fullest extent permitted by the law, We disclaim all warranties (whether express or implied) including but not limited to warranties that the Prizes will meet the Prize winners’ requirements, warranties of satisfactory quality, warranties of merchantability or fitness for a particular purpose. All warranty claims in regards to a Prize should be directed to the applicable manufacturer, supplier and/or distributor.</p>
									</li>
									<li data-aos="fade-up">
										<p>We have the right to disqualify a Prize winner and forfeit any Prize if:-</p>
										<ol>
											<li data-aos="fade-up">
												<p>the Prize winner has committed any act of fraud or dishonesty, or has misrepresented anything in connection with the Campaign;</p>
											</li>
											<li data-aos="fade-up">
												<p>we are unable to contact the Prize winner using the contact information provided to us;</p>
											</li>
											<li data-aos="fade-up">
												<p>the Prize winner does not respond to us or notify us that he or she does not wish to claim the Prize;</p>
											</li>
											<li data-aos="fade-up">
												<p>the Prize winner does not agree to abide by and be bound by these terms; or</p>
											</li>
											<li data-aos="fade-up">
												<p>the Prize winner dies or becomes mentally incapable</p>
											</li>
										</ol>
										<p>If a Prize winner is disqualified, we may in our sole discretion draw another name to replace the disqualified Prize winner or donate the Prize in question.</p>
									</li>
									<li data-aos="fade-up">
										<p>All notices or communications given by us to you will be deemed to be received or notified to you on the date of broadcast via electronic media such as SMS and e-mails.</p>
									</li>
									<li data-aos="fade-up">
										<p>All Prize winners will be required to collect their prizes at 50 Scotts Road, #04-03, Singapore 228242, between Monday to Friday, 10 AM – 5 PM, excluding public holidays, by 13 December 2019. If you are unable to be physically present to claim your Prize, you may authorise another person to claim the Prize on your behalf with an authorisation letter. Prizes which remain unclaimed after two (2) months after the announcement of the prize winners will be forfeited.</p>
									</li>
									<li data-aos="fade-up">
										<p>By participating in the Campaign and/or claiming any Prize, the participant hereby releases and agrees to hold harmless, subject to the maximum extent permitted under the law, us and our related corporations and affiliates (including respective officers, directors, employees and agents) from any and all liability for any loss (including, without limitation, indirect or consequential loss), damage or expenses in connection with the Campaign, or any Prize.</p>
									</li>
									<li data-aos="fade-up">
										<p>You accept that our decision arising out of or in connection with any matters pertaining to the Campaign (including the award of Prizes) is final and binding. We are not obliged to enter into any correspondence with any participant of the Campaign. No enquiries, appeals, verbal or written, shall be entertained.</p>
									</li>
									<li data-aos="fade-up">
										<p>In the event of any inconsistency between these Campaign Terms and any brochure, marketing or promotional material relating to the Campaign, these Campaign Terms will prevail.</p>
									</li>

								</ol>
							</li>
							<h5>PART C - ADDITIONAL TERMS IN RELATION TO TGSR FAN CAMPAIGN</h5>
							<li data-aos="fade-up">
								<h5 class="title-blue">ABOUT THE CAMPAIGN</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>In addition to the terms in Part A above, the terms in this Part C (collectively, “TGSR Fan Campaign Terms”) applies to you if you participate in the TGSR Fan Campaign (“TGSR Fan Campaign”).</p>
									</li>
									<li data-aos="fade-up">
										<p>The TGSR Fan Campaign will be made available on our Website, and will commence from 12 December 2019, 00:00 hours and end on 15 January 2020, 23:59 hours (“TGSR Fan Campaign Period”). The TGSR Fan Campaign comprises:</p>
										<ol>
											<li data-aos="fade-up">
												<p>weekly campaign, which refers to the assessment of top three (3) participants who have accumulated the most riffs at the end of each qualifying period (as defined in Clause 17.3 below)(“Weekly Fan Campaign”); and</p>
											</li>
											<li data-aos="fade-up">
												<p>grand campaign, which refers to the assessment of the top three (3) participants who have accumulated the most riffs at the end of the TGSR Fan Campaign Period (“Grand Fan Campaign”).</p>
											</li>
										</ol>
									</li>
									<li data-aos="fade-up">
										<p>The details of the Weekly Fan Campaign are set out below:-</p>
										<ul>
											<li> <strong>Weekly Fan Campaigns : </strong> 
											Weekly Fan Campaign 1</li>
											<li> <strong>Qualifying Period : </strong> 
											12 December 2019 00:00 hours to 18 December 2019 23:59 hours</li>
										</ul>
										<ul>
											<li> <strong>Weekly Fan Campaigns : </strong> 
											Weekly Fan Campaign 2</li>
											<li> <strong>Qualifying Period : </strong> 
											19 December 2019 00:00 hours to 25 December 2019 23:59 hours</li>
										</ul>
										<ul>
											<li> <strong>Weekly Fan Campaigns : </strong> 
											Weekly Fan Campaign 3</li>
											<li> <strong>Qualifying Period : </strong> 
											26 December 2019 00:00 hours to 1 January 2020 23.59 hours</li>
										</ul>
										<ul>
											<li> <strong>Weekly Fan Campaigns : </strong> 
											Weekly Fan Campaign 4</li>
											<li> <strong>Qualifying Period : </strong> 
											2 January 2020 00:00 hours to 8 January 2020 23:59 hours</li>
										</ul>
										<ul>
											<li> <strong>Weekly Fan Campaigns : </strong> 
											Weekly Fan Campaign 5</li>
											<li> <strong>Qualifying Period : </strong> 
											9 January 2020 00:00 hours to 15 January 2020 23:59 hours</li>
										</ul>
									</li>
									<li data-aos="fade-up">
										<p>By participating in the TGSR Fan Campaign, you agree to be bound by and to comply with the TGSR Fan Campaign Terms. We may, at our sole discretion and without prior notice, vary, modify or amend the TGSR Fan Campaign Terms. The amended TGSR Fan Campaign Terms will replace the original TGSR Fan Campaign Terms.</p>
									</li>
									<li data-aos="fade-up">
										<p>We reserve the right to withdraw, discontinue, postpone or terminate the TGSR Fan Campaign without prior notice. We shall not be liable for any loss, damage or expenses incurred as a result.</p>
									</li>
									<li data-aos="fade-up">
										<p>Non-compliance with or breach of any of these TGSR Fan Campaign Terms may disqualify your participation in this TGSR Fan Campaign, and any prizes won may be forfeited, withheld, withdrawn or reclaimed by us.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">ACCOUNT REGISTRATION</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>You are required to apply and register for an account with us before you are entitled to participate in the TGSR – Fan Campaign (“User Account”). In order to sign up for a User Account, you will need to register by:-</p>
										<ol>
											<li data-aos="fade-up">
												<p>using either your (a) Facebook login credentials by clicking on the Facebook login button; (b) Google account credentials by clicking on the Google login button; and</p>
											</li>
											<li data-aos="fade-up">
												<p>providing us with such other personal data that we may request from you (e.g. your e-mail address).</p>
											</li>
										</ol>
									</li>
									<li data-aos="fade-up">
										<p>Persons in the following categories are not eligible to participate in the TGSR Fan Campaign:-</p>
										<ol>
											<li data-aos="fade-up">
												<p>all persons identified in Clause 10.1 above; and</p>
											</li>
											<li data-aos="fade-up">
												<p>persons who do not reside in Singapore.</p>
											</li>
										</ol>
									</li>
									<li data-aos="fade-up">
										<p>In signing up for a User Account with us, you represent and warrant that:</p>
										<ol>
											<li data-aos="fade-up">
												<p>all registration information provided by you is true, accurate, current and complete to the best of your knowledge and belief;</p>
											</li>
											<li data-aos="fade-up">
												<p>you are not a person who is ineligible to participate in the TGSR Fan Campaign pursuant to Clause 18.2 above;</p>
											</li>
											<li data-aos="fade-up">
												<p>you will promptly update changes to your registration details; and</p>
											</li>
											<li data-aos="fade-up">
												<p>you are of legal age and have the requisite mental and legal capacity in accordance with the applicable laws of Singapore to enter into these TGSR Fan Campaign Terms and participate in the TGSR Fan Campaign</p>
											</li>
										</ol>
									</li>
									<li data-aos="fade-up">
										<p>We reserve the right to reject your application for a User Account, suspend your use of or terminate your User Account.</p>
										<p style={customStyle}><i>Maintenance of User Account</i></p>
									</li>
									
									<li data-aos="fade-up">

										<p>Upon successful registration of your User Account, you, as the account holder:</p>
										<ol>
											<li data-aos="fade-up">
												<p>agree to keep your User Account confidential and shall not allow other person to use the User Account; and</p>
											</li>
											<li data-aos="fade-up">
												<p>shall notify us immediately if you have any reason to believe that the security of your User Account has been compromised.</p>
											</li>
										</ol>
									</li>
									<li data-aos="fade-up">
										<p>You are solely responsible for any and all activities which occur under your User Account. We are entitled to treat all activities that occur under your User Account as having been conducted with your knowledge and authority. For the avoidance of doubt, in cases where you have allowed any other person to use your User Account or have negligently or otherwise made your User Account details publicly available, you agree that you are fully responsible for (a) the online conduct of such user; and (b) the consequences of any use or misuse.</p>
									</li>
									<li data-aos="fade-up">
										<p>You acknowledge and agree that we may access your User Account and its contents as necessary for purposes including but not limited to identifying or resolving technical problems or responding to complaints without prior notice to you.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">EARNING RIFFS</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>During the TGSR Fan Campaign Period, you may accumulate riffs (“Riffs”) upon the completion of specific tasks (“Eligible Tasks”) as may be solely determined by us. The amount of Riffs that will be awarded and credited to your User Account are set out in the table below:      </p>
										<ul>
											<li> <strong>Eligible Task : </strong> Attend the Showcase Concert on 18 Jan 2020 and scan the QR code</li>
											<li> <strong>Riffs : </strong> 500</li>
										</ul>
										<ul>
											<li> <strong>Eligible Task : </strong> Spot TGSR SMRT trains on the main and circle lines and scan the QR code on the TGSR promotion window stickers</li>
											<li> <strong>Riffs : </strong> 300</li>
										</ul>
										<ul>
											<li> <strong>Eligible Task : </strong> Visit booths at the Showcase Concert and scan the QR codes</li>
											<li> <strong>Riffs : </strong> 200</li>
										</ul>
										<ul>
											<li> <strong>Eligible Task : </strong> Attend any of the pop-up performances from 23 Nov to 21 Dec, and scan the QR code</li>
											<li> <strong>Riffs : </strong> 150</li>
										</ul>
										<ul>
											<li> <strong>Eligible Task : </strong> Sign up on the events page</li>
											<li> <strong>Riffs : </strong> 50</li>
										</ul>
										<ul>
											<li> <strong>Eligible Task : </strong> Watch at least 30 seconds of a video on the website</li>
											<li> <strong>Riffs : </strong> 30</li>
										</ul>
										<ul>
											<li> <strong>Eligible Task : </strong> View a photo album (limited to one per day)</li>
											<li> <strong>Riffs : </strong> 20</li>
										</ul>
										<ul>
											<li> <strong>Eligible Task : </strong> View a webpage (limited to one per day)</li>
											<li> <strong>Riffs : </strong> 10</li>
										</ul>
										<ul>
											<li> <strong>Eligible Task : </strong> Find a bonus boombox anywhere on the website</li>
											<li> <strong>Riffs : </strong> 100</li>
										</ul>
									</li>
									<li data-aos="fade-up">
										<p>You may review and verify the following information on our Website:-</p>
										<ol>
											<li data-aos="fade-up">
												<p>the total amount of Riffs that you have accumulated in the course of the TGSR Fan Campaign Period, and each Weekly Fan Campaign; and</p>
											</li>
											<li data-aos="fade-up">
												<p>your approximate position in the leaderboard for that week’s Weekly Fan Campaign, and the overall TGSR Fan Campaign.</p>
											</li>
										</ol>
									</li>
									<li data-aos="fade-up">
										<p>The total amount of Riffs associated with your User Account and as displayed on the Website shall serve as conclusive evidence.</p>
									</li>
									<li data-aos="fade-up">
										<p>We may, at our sole discretion, vary the Riffs to be awarded for each Eligible Task as set out in Clause 19.1 above, as we may notify you from time to time. We are under no obligation to provide you with any explanation whatsoever regarding the quantum, value and/or computation methodology of the Riffs.</p>
									</li>
									<li data-aos="fade-up">
										<p>The Riffs do not have any cash or monetary value. All Riffs awarded to you and associated with your User Account are non-transferrable. For the avoidance of doubt, the Riffs shall under no circumstances be regarded as valuable or exchangeable instruments.</p>
									</li>
									<li data-aos="fade-up">
										<p>We have the sole discretion to determine the validity of the Riffs earned on your User Account. We may terminate your User Account if you have obtained any Riffs as a result of any hacking, compromise and/or breach of our IT systems (whether inadvertently or intentionally), or in breach of any of these terms.</p>
									</li>
									<li data-aos="fade-up">
										<p>We also reserve the right, at any time, to invalidate any User Accounts that we suspect to have been created using false, incorrect, fraudulent or misleading information, including but not limited to personal details and contact information, and/or Riffs earned through the use of multiple identities, email addresses or Facebook / Google accounts.</p>
									</li>
									<li data-aos="fade-up">
										<p>We may suspend the calculation of accrual of Riffs for any reason whatsoever without prior notice to you.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">CONDUCT OF TGSR FAN MUSICAL CAMPAIGN</h5>
								<p><i>Weekly Fan Campaign</i></p>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>The top three (3) participants who accumulate the most Riffs at the expiry of each Qualifying Period shall win the following prizes (“Fan Prizes”):</p>
										<ul>
											<li> <strong>Weekly Fan Campaign : </strong> Weekly Fan Campaign 1</li>
											<li> <strong>Fan Prizes : </strong> Beats EP On-Ear Headphones [black]</li>
										</ul>
										<ul>
											<li> <strong>Weekly Fan Campaign : </strong> Weekly Fan Campaign 2</li>
											<li> <strong>Fan Prizes : </strong> Ultimate Ears WONDERBOOM 2 Portable Wireless Bluetooth Speaker [grey]</li>
										</ul>
										<ul>
											<li> <strong>Weekly Fan Campaign : </strong> Weekly Fan Campaign 3</li>
											<li> <strong>Fan Prizes : </strong> Alexa Echo Smart Speaker [light grey]</li>
										</ul>
										<ul>
											<li> <strong>Weekly Fan Campaign : </strong> Weekly Fan Campaign 4</li>
											<li> <strong>Fan Prizes : </strong> JBL Tune 120TWS Truly Wireless Earbuds [green]</li>
										</ul>
										<ul>
											<li> <strong>Weekly Fan Campaign : </strong> Weekly Fan Campaign 5</li>
											<li> <strong>Fan Prizes : </strong> Crosley Cruiser Deluxe Portable Turntable [white sand]</li>
										</ul>
										<p>For the avoidance of doubt, any winner(s) of the Weekly Fan Campaign will not be precluded from participating in any of the subsequent Weekly Fan Campaign(s) and/or the Grand Fan Campaign.</p>
									</li>
									<li data-aos="fade-up">
										<p>Each week’s Weekly Campaign winner(s) will be notified via the e-mail address that we have obtained at the point of registration (e.g. through the e-mail address associated with your facebook/google ID). The results for each Weekly Fan Campaign will also be announced on our Facebook page (<a href="https://www.facebook.com/thegreatsingaporereplay/">https://www.facebook.com/thegreatsingaporereplay/</a>) within seven (7) working days from the expiry of each Qualifying Period.</p>
									</li>
									<li data-aos="fade-up">
										<p>In respect of the Weekly Fan Campaign, each participant’s number of Riffs will only accrue for that Qualifying Period in which the Riffs were earned, and will not be carried forward to the subsequent Weekly Fan Campaign. Notwithstanding the foregoing, all Riffs accrued across all Weekly Fan Campaigns will count towards the Grand Fan Campaign.</p>
									</li>
									<p style={customStyle2}><i>Grand Fan Campaign</i></p>
								</ol>
								<li data-aos="fade-up">
									<p>The top three (3) participants who accumulate the most Riffs at the expiry of the TGSR Fan Musical Campaign Period shall win the following Fan Prizes:</p>
									<ul>
										<li> <strong>Grand Fan Campaign : </strong> First Prize</li>
										<li> <strong>Fan Prizes : </strong> Bose Wave® SoundTouch® music system IV</li>
									</ul>
									<ul>
										<li> <strong>Grand Fan Campaign : </strong> Second Prize</li>
										<li> <strong>Fan Prizes : </strong> Beoplay H9i Wireless Bluetooth Over-Ear Headphones with Active Noise Cancellation, Transparency Mode and Microphone [limestone]</li>
									</ul>
									<ul>
										<li> <strong>Grand Fan Campaign : </strong> Third Prize</li>
										<li> <strong>Fan Prizes : </strong> Popsical Remix</li>
									</ul>
								</li>
								<li data-aos="fade-up">
									<p>The winner(s) of the Grand Fan Campaign will be notified via the e-mail address that we have obtained at the point of registration (e.g. through the e-mail address associated with your facebook/google ID). The results for the Grand Fan Campaign will also be announced on our Facebook page (<a href="https://www.facebook.com/thegreatsingaporereplay/">https://www.facebook.com/thegreatsingaporereplay/</a>) within seven (7) working days from the expiry of the TGSR Fan Musical Campaign Period.</p>
									<p><i>Applicable to both Weekly Fan Campaign and Grand Fan Campaign</i>
									</p>
								</li>
								<li data-aos="fade-up">
									<p>In the event that there is a tie as between two or more participants who have accumulated the same number of Riffs, the winner shall be determined by the first to obtain that number of Riffs based on our transaction timestamp.</p>
								</li>
								<li data-aos="fade-up">
									<p>We have the right to disqualify a Fan Prize winner and forfeit any Fan Prize if:-</p>
									<ol>
										<li data-aos="fade-up">
											<p>the Prize winner has committed any act of fraud or dishonesty, or has misrepresented anything in connection with the TGSR Fan Campaign;</p>
										</li>
										<li data-aos="fade-up">
											<p>we are unable to contact the Fan Prize winner using the contact information provided to us;</p>
										</li>
										<li data-aos="fade-up">
											<p>the Fan Prize winner does not respond to us or notify us that he or she does not wish to claim the Fan Prize;</p>
										</li>
										<li data-aos="fade-up">
											<p>the Fan Prize winner does not agree to abide by and be bound by these terms; or</p>
										</li>
										<li data-aos="fade-up">
											<p>the Fan Prize winner dies or becomes mentally incapable.</p>
										</li>
										<p>If a Fan Prize winner is disqualified, we may in our sole discretion draw another name to replace the disqualified Fan Prize winner or donate the Fan Prize in question.</p>
									</ol>
								</li>
								<li data-aos="fade-up">
									<p>All notices or communications given by us to you will be deemed to be received or notified to you on the date of broadcast via electronic media such as e-mails and Facebook messages.</p>
								</li>
								<li data-aos="fade-up">
									<p>All Fan Prize winners will be required to collect their prizes at 50 Scotts Road, #04-03, Singapore 228242, between Monday to Friday, 10 AM – 5 PM, excluding public holidays, within one month of notifying the winners. If you are unable to be physically present to claim your Fan Prize, you may authorise another person to claim the Fan Prize on your behalf with an authorisation letter. Fan Prizes which remain unclaimed after two (2) months after the announcement of the winners will be forfeited.</p>
								</li>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">FAN PRIZES</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>The Fan Prizes are not replaceable (whether in whole or in part), exchangeable or redeemable for cash or in-kind</p>
									</li>
									<li data-aos="fade-up">
										<p>The images of Fan Prizes on all marketing materials are for illustration purposes only. Actual products may be different from the images shown. Fan Prizes cannot be assigned, transferred, redeemed for cash or substituted. We will not replace any lost or stolen Fan Prizes.</p>
									</li>
									<li data-aos="fade-up">
										<p>We may, in our sole and absolute discretion and without prior notice, substitute, withdraw, add to or alter any of the Fan Prizes offered with another of comparable value if the Fan Prize is unavailable.</p>
									</li>
									<li data-aos="fade-up">
										<p>The value of the Fan Prize shall be solely determined by us.</p>
									</li>
									<li data-aos="fade-up">
										<p>The Fan Prizes may be subject to the terms and conditions of the respective third party manufacturer, supplier and/or distributor (if any). We shall not be liable for any separate terms and conditions as may be imposed by such third parties for the use of the Fan Prizes.</p>
									</li>
									<li data-aos="fade-up">
										<p>Taxes, if any, related to a Fan Prize are the sole responsibility of the Fan Prize winner. We accept no responsibility for any tax or other liability that may arise from the Fan Prize winnings.</p>
									</li>
									<li data-aos="fade-up">
										<p>The Fan Prizes are provided on “as is”, “with all faults” and “as available” basis. We do not warrant, represent or guarantee the usability or quality of the Fan Prizes that the claims made by the manufacturers, suppliers and/or distributors of those Fan Prizes are accurate. To the fullest extent permitted by the law, We disclaim all warranties (whether express or implied) including but not limited to warranties that the Fan Prizes will meet the Fan Prize winners’ requirements, warranties of satisfactory quality, warranties of merchantability or fitness for a particular purpose. All warranty claims in regards to a Fan Prize should be directed to the applicable manufacturer, supplier and/or distributor.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">PERSONAL DATA</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>All information furnished must be accurate and complete, or it will result in an automatic disqualification.</p>
									</li>
									<li data-aos="fade-up">
										<p>By participating in the TGSR Fan Campaign, you agree that any and all personal data may be collected, used, disclosed and processed in accordance with the TGSR Privacy Policy, as well as for the following purposes: (a) facilitating and administering the TGSR Fan Campaign; and (b) delivery of the Prizes (including contacting you for the administration of the Prizes). In particular, you acknowledge and agree that we may publish the username of the winner(s) on our Facebook Page for marketing and/or promotional purposes.</p>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">TERMINATION OF USER ACCOUNTS</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>You may request to deactivate your User Account at any time.</p>
									</li>
									<li data-aos="fade-up">
										<p>We may, at our sole discretion, with or without prior notice to you, suspend or terminate your User Account with immediate effect for any reason.</p>
									</li>
									<p style={customStyle2}><i>Effect of Termination</i></p>
									<li data-aos="fade-up">
										<p>If we discontinue the TGSR Fan Campaign, terminate your User Account or you choose to deactivate your User Account:</p>
										<ol>
											<li data-aos="fade-up">
												<p>all Riffs that have been accrued as of the date of termination will be forfeited; and</p>
											</li>
											<li data-aos="fade-up">
												<p>we shall deactivate your User Account and all related information in such account without liability to you.</p>
											</li>
										</ol>
									</li>
								</ol>
							</li>
							<li data-aos="fade-up">
								<h5 class="title-blue">MISCELLANEOUS</h5>
								<ol class="sublist">
									<li data-aos="fade-up">
										<p>By participating in the TGSR Fan Campaign and/or claiming any Fan Prize, the participant hereby releases and agrees to hold harmless, subject to the maximum extent permitted under the law, us and our related corporations and affiliates (including respective officers, directors, employees and agents) from any and all liability for any loss (including, without limitation, indirect or consequential loss), damage or expenses in connection with the TGSR Fan Campaign, or any Fan Prize.</p>
									</li>
									<li data-aos="fade-up">
										<p>You accept that our decision arising out of or in connection with any matters pertaining to the TGSR Fan Campaign (including the award of Fan Prizes) is final and binding. We are not obliged to enter into any correspondence with any participant of the TGSR Fan Campaign. No enquiries, appeals, verbal or written, shall be entertained.</p>
									</li>
									<li data-aos="fade-up">
										<p>In the event of any inconsistency between these TGSR Fan Campaign Terms and any brochure, marketing or promotional material relating to the TGSR Fan Campaign, these TGSR Fan Campaign Terms will prevail.</p>
									</li>
								</ol>
							</li>
						</ol>
					</div>
				</div>
			</div>
	);
};


// producers
export default Tnc;
