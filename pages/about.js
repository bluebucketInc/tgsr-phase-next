// imports
import React, { useEffect } from "react";
import Link from 'next/link';
import { NextSeo } from 'next-seo';
import fetch from "isomorphic-unfetch";

// styles
import "@styles/artists.scss";

// component
const About = ({ artists, mentions }) => {

    // add body class
    useEffect(() => {
        document.getElementsByTagName("body")[0].className = "about-background";
    }, [0]);

    const style1 = {
        marginLeft: -16
    }

    const style2 = {
        marginLeft: -40
    }
    // render
    return (
        <div class="about-page body-contents">
      <div class="container-2">
        <div class="page-title" data-aos="fade-up">
          <h5>ABOUT THE PROGRAMME</h5>
          <h3>THE GREAT SINGAPORE REPLAY: SEASON 2</h3>
        </div>
        <div class="introduction" data-aos="fade-up">
          <h5 class="title-blue">Reliving Singapore's History Through Music</h5>
          <p>Presented by Temasek, the biennial The Great Singapore Replay (TGSR) is about discovery and collaboration - discovery of Singapore's rich music heritage and the collaboration between established artists and the next generation of aspiring young local talents to create original 'Made-in-Singapore' songs.</p>
          <p>In its second season, TGSR offers 10 music talents the opportunity to be guided by five established artists in creating 10 new original songs. These songs will be inspired by mashups of distinct tunes, voted by the public, from five iconic eras of Singapore's history and five modern music genres.</p>
          <p>Discovery and collaboration have always been the heart and soul of TGSR, and this season will have no shortage of either. Expect to see an even bigger collaboration of artists and industry professionals, in this continued effort to nurture Singapore's music scene and showcase our diversity of aspiring young talents.</p>
          <p>Time to tune in, because music history is about to be written.</p>
          <p>Working with local music industry players, the selected talents will undergo a professional production experience, comprising song-writing workshop, song arrangement and recording. It will also include the marketing and creating of individual artists' brand, and the launch of their songs on popular digital music streaming platforms.</p>
        </div>
        <h5 class="title-pink" data-aos="fade-up">Presented By</h5>
        <div class="row partner-block" data-aos="fade-up">
          <div class="col-12 col-sm-12 col-md-4">
            <div class="partner-logos" data-aos="fade-up"><img src="clientlibs/images/global/logo-temasek@2x.png" /></div>
          </div>
          <div class="col-12 col-sm-12 col-md-8" data-aos="fade-up">
            <p>Temasek is an investment company with a net portfolio value of S$313 billion as at 31 March 2019. Our Temasek Charter roles as an investor, institution and steward shape our investment stance, ethos and philosophy, to do well, do right and do good. We actively seek sustainable solutions to address present and future challenges, as we capture investment and other opportunities that help to bring about a better, smarter and more connected world. As a Trusted Steward, Temasek has gifted 19 philanthropic endowments that make possible outcome-focused programmes that enable individuals, families and communities to achieve sustainable improvements and progress in their lives.</p><a href="https://www.temasek.com.sg">www.temasek.com.sg.</a>
            <div class="d-flex flex-row social-icons"><a href="https://www.facebook.com/temasekholdings" target="_blank"><img src="clientlibs/images/global/icon-facebook@2x.png" /></a><a href="https://instagram.com/temasekseen/" target="_blank"><img src="clientlibs/images/global/icon-instagram@2x.png" /></a><a href="https://twitter.com/Temasek" target="_blank"><img src="clientlibs/images/global/icon-twitter@2x.png" /></a><a href="https://www.youtube.com/user/TemasekDigital" target="_blank"><img src="clientlibs/images/global/icon-youtube@2x.png" /></a></div>
          </div>
        </div>
        <div class="support-title">
          <h5 class="title-pink" data-aos="fade-up">Supported By</h5>
          <div class="row">
            <div class="col-6 col-sm-6 col-md-4">
              <div class="special-logos" data-aos="flip-up"><img src="clientlibs/images/logos/logo-sony@2x.png"/></div>
            </div>
            <div class="col-6 col-sm-6 col-md-4">
              <div class="special-logos" data-aos="flip-up"><img src="clientlibs/images/logos/logo-universal@2x.png"/></div>
            </div>
            <div class="col-6 col-sm-6 col-md-4">
              <div class="special-logos" data-aos="flip-up"><img src="clientlibs/images/logos/logo-warner@2x.png" style={style1}/></div>
            </div>
            <div class="col-6 col-sm-6 col-md-4">
              <div class="special-logos" data-aos="flip-up"><img src="clientlibs/images/logos/logo-warrierrecords@2x.png"/></div>
            </div>
            <div class="col-6 col-sm-6 col-md-4">
              <div class="special-logos" data-aos="flip-up"><img src="clientlibs/images/logos/logo-zendyll@2x.png" style={style2}/></div>
            </div>
          </div>
        </div>
        <div class="support-title bicentennial">
          <h5 class="title-pink" data-aos="fade-up">In Support Of</h5>
          <div class="row">
            <div class="col-6 col-sm-6 col-md-4">
              <div class="special-logos" data-aos="flip-up"><img src="clientlibs/images/global/big-logo-SGBicentennial@2x.png"/></div>
            </div>
          </div>
          <h5 class="title-pink" data-aos="fade-up">Venue Partner</h5>
          <div class="row">
            <div class="col-6 col-sm-6 col-md-4">
              <div class="special-logos" data-aos="flip-up"><img src={require(`@images/about/garden.png`)}/></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
};


// producers
export default About;
