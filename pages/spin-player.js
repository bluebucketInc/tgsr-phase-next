// imports
import React, { useEffect } from "react";

// component
const spinPlayer = () => {

	var iframeStyle = {
		'width': '100%',
		'border': '0',
		'height': '850px',
	};

	useEffect(() => {
		$('.spot-lights').css('display', 'none');
	}, [0]);

	// render
	return (
		<iframe src="spin-player/index.html" style={iframeStyle}></iframe>
	);
};


// producers
export default spinPlayer;
