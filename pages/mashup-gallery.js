// imports
import React  from "react"
import Slider from 'react-slick'
import { NextSeo } from 'next-seo';
import { getURlParam } from '../libs/utils'
import { Boombox } from "@components/hidden-boombox";
import { jwtEncodeData } from "@components/jwt-verify";

// styles
import "@styles/mashups.scss";

// component
import { Button } from "@components/button";

// overlay
class MashupPage extends React.Component {

	// initial props
	static async getInitialProps() {
		// dynamicaly import producer content

		var request ={
			"page_url": "mash_up"
		}

		const res = await fetch(process.env.REACT_APP_API_SERVER_URL + 'get-page-boomboxes-list', {
			method: 'post',
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: jwtEncodeData(request)
		});

		let bbFlagList = {};
		const response = await res.json();

		if(response.status == 1) {
			for (var i = response.list.length - 1; i >= 0; i--) {
				bbFlagList[response.list[i]['section_id'].toLowerCase()] = {
					'boombox_id': response.list[i]['boombox_id'],
					'is_real'	: (response.list[i]['is_real'] == 1 ? 'correct' : 'wrong')
				};
			}
		}

		const data = await import("@content/mashups.json");
		return { mashups: data.default.mashups, bbFlagList: bbFlagList };
	}

	// add body class
	componentDidMount() {
		// add class
		document.getElementsByTagName("body")[0].className = "mashups-page-body";
		// move to slider
		setTimeout(()=>getURlParam("id") && this.slider.slickGoTo(parseInt(getURlParam("id"))), 200);
	}

	// render
	render() {
		// slider settings
		const slickSettings = {
			className: "mashup-carousell",
			dots: true,
			infinite: false,
			speed: 600,
			slidesToShow: 1,
			slidesToScroll: 1,
			dotsClass: 'slick-dots onboarding-dots',
		};
		// render
		return (
			<div className="container mashups-page">
				<NextSeo title="The Great Singapore Replay: Season 2: TIMES AND TUNES" />
				<Slider
					ref={slider => (this.slider = slider)}
					{...slickSettings}
				>
					{
						this.props.mashups.map((item, index) =>
							<div className="slides" key={item.left+index} data-hash={item.id}>
								<div className="row">
									<div className="col-sm-12 text-center header-wrapper">
										<div className="heading">
											<h4>TIMES AND TUNES</h4>
										</div>
										<div className="genre-wrapper">
											<h1>{item.left}</h1>
											<img
												className="genre-x"
												src={require("@images/logo/x.png")}
												alt="Mashup"
											/>
											<h1>{item.right}</h1>
										</div>
									</div>
									<div className="col-md-5 col-sm-12 image-wrapper">
										<img
											className="genre-image img-responsive"
											src={require(`@images/mashups/${item.img}`)}
											alt={`${item.left}x${item.right}`}
										/>
									</div>
									<div className="col-md-7 col-sm-12 text-wrapper my-auto">
										{
											item.desc.map(p => (
												<p key={p}>{p}</p>
											))
										}

										<div className="bottom-wrapper">
											<p><strong>This mashup will be used to inspire a song by:</strong></p>
											<Button classes="long-btn" href={item.artist.link}>{item.artist.name}</Button>

											{
												typeof this.props.bbFlagList[item.id] == 'object' && <Boombox type={this.props.bbFlagList[item.id]['is_real']} onClick={() => boomboxClicked()} bbID={this.props.bbFlagList[item.id]['boombox_id']} />
											}
										</div>

									</div>
								</div>
							</div>
						)
					}
				</Slider>
			</div>
		);
	}
};

// export
export default MashupPage;
