import React from 'react'
import SocialLogin from 'react-social-login'
 
class SocialButton extends React.Component {
 
    render() {
        return (
            <p className={this.props.classes} onClick={this.props.triggerLogin} {...this.props}>
                { this.props.children }
            </p>
        );
    }
}
 
export default SocialLogin(SocialButton);