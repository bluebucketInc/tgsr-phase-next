// imports
import { Fragment } from 'react';
import Link from 'next/link';
import { toast, ToastContainer } from "react-toastify";
import { getRandomInt } from "@utils";

// styles
import "@styles/toast.scss";

// toast settings
export const t_settings = {
	position: toast.POSITION.BOTTOM_CENTER,
	position: "bottom-center",
	autoClose: 5000,
	closeOnClick: false,
	closeButton: false,
	pauseOnHover: true,
	hideProgressBar: true,
	draggable: false,
	className: "boombox-toast"
};

// toast container
export const ToastWrapper = () => <ToastContainer {...t_settings} />

// toast
export const PointsToast = ({ type, points, closeToast, eventMessage, dynamicMessage }) => {
	// rand
	const rand = getRandomInt(1, 3);
	// render
	return (
		<div className="toast-wrapper">
			<img
				src={require(`@images/toast/points/boombox-${rand}.png`)}
				alt="BoomBox"
				className="boombox"
			/>
			<img
				src={require("@images/toast/points/close.png")}
				alt="Close"
				onClick={closeToast}
				className="close-btn"
			/>
			<div className="point-container"><p>+ {points} RIFFS</p></div>
				<p className="message">{eventMessage}<br/>{dynamicMessage}</p>
			</div>
	);
};

// toast
export const AlertToast = ({ type, points, closeToast, showMessage, extraClass }) => {

	extraClass = "close-btn close-btn-" + $('#login_type').text();

	// rand
	const rand = getRandomInt(1, 3);
	// render
	return (
		<div className="toast-wrapper alert">
			<img
				src={require(`@images/toast/alert/boombox-${rand}.png`)}
				alt="BoomBox"
				className="boombox"
			/>
			<img
				src={require("@images/toast/alert/close.png")}
				alt="Close"
				onClick={closeToast}
				className={extraClass}
			/>
			<p className="message">{showMessage}</p>
			{
				type === "signin"
				? 
					<a href={`/gamification`} className="link toast_find_out_how">Find Out How</a>
					
				: <div className="point-container"><p>+ {points} RIFFS</p></div>
			}
		</div>
	);
};

// notifications
const Message = (type) => {
	switch (type) {
		case "pageview":
			return <Fragment>You’ve Viewed a webpage,<br />Keep grooving!</Fragment>;
		case "video":
			return <Fragment>You’ve watched a video,<br />rock on!</Fragment>;
		case "signin":
			return <Fragment>sign in to rack up riffs<br />and WIN AMAZING PRIZES!</Fragment>;
		case "al_video":
			return <Fragment>watch videos and<br />rack up riffs</Fragment>;
		default:
			return null;
	}
}

