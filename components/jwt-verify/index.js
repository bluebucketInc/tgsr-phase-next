import { Fragment, useState } from "react";
import jwtSimple from "jwt-simple";

// thumbnail
export const jwtEncodeData = (dataObject) => {
	return JSON.stringify({'token': jwtSimple.encode(dataObject, process.env.ARG_V)});
};