// imports
import Header from "../_partials/Header";
import Navigation from "../_partials/Navigation";
import Footer from "../_partials/Footer";

// component
const Layout = props => (
	<div>
		{/* GTM NO-SCRIPT */}
		<noscript>
			<iframe
				src="https://www.googletagmanager.com/ns.html?id=GTM-538D4FF"
				height="0"
				width="0"
				style={{display: "none", visibility:"hidden"}}>
			</iframe>
		</noscript>
		<Header />
		<Navigation />
		{props.children}
		<Footer />
	</div>
);

// export
export default Layout;