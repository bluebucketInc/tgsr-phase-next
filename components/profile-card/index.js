
// styles
import "@styles/card.scss";

// component
const Card = ({ data, onLogout }) => {
    // render
    return (
        <div className="drop-menu">
            <div className="menu">
            {
                data.activeweek > 0 &&
                <div className="submenu">
                    This Week:
                    <div className="container">
                        <img
                            src={require("@images/navigation/icon_rank@2x.png")}
                            width="20px"
                            height="16px"
                        />
                        <span className="number">{data.fans}</span>
                        <span className="desc">Rock Stars ahead</span>
                    </div>
                    <div className="container">
                        <img
                            src={require("@images/navigation/icon_progress@2x.png")}
                            width="19px"
                            height="13px"
                        />
                        <span className="number">{data.riffstogo}</span>
                        <span className="desc">
                            Riffs to this week’s Top 3
                        </span>
                    </div>
                </div>
            }
                <div className="submenu">
                    All Weeks:
                    <div className="container">
                        <img
                            src={require("@images/navigation/icon_riffs_grey@2x.png")}
                            width="13px"
                            height="17px"
                        />
                        <span className="number">{data.riffs}</span>
                        <span className="desc">Riffs on your scoresheet</span>
                    </div>
                    <div className="container">
                        <img
                            src={require("@images/navigation/icon_rank@2x.png")}
                            width="20px"
                            height="16px"
                        />
                        <span className="number">{data.totalFans}</span>
                        <span className="desc">Rock Stars ahead</span>
                    </div>
                    <div className="container">
                        <img
                            src={require("@images/navigation/icon_progress@2x.png")}
                            width="19px"
                            height="13px"
                        />
                        <span className="number">{data.totalRiffstogo}</span>
                        <span className="desc">
                            Riffs to the ultimate Top 3
                        </span>
                    </div>
                </div>
                <div className="submenu">
                    <div className="container last-section sign-out" onClick={onLogout}>
                        Sign Out
                    </div>
                    <div className="earn-more"><a href="/gamification">Earn More Riffs</a></div>
                   
                </div>
            </div>
        </div>
    );
};

// export
export default Card;
