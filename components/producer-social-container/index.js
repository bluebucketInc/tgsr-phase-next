// imports
import React from "react";

// component
const SocialContainer = ({ fb, ig, yt }) => (
    <div className="social-container">
        {fb && (
            <a href={fb} target="_blank">
                <img src={require("@images/logo/facebook-2x.png")} />
            </a>
        )}
        {ig && (
            <a href={ig} target="_blank">
                <img src={require("@images/logo/instagram-2x.png")} />
            </a>
        )}
        {yt && (
            <a href={yt} target="_blank">
                <img style={{width: 42}} src={require("@images/logo/youtube.png")} />
            </a>
        )}
    </div>
);

// export
export default SocialContainer;
