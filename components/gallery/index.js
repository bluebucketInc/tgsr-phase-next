// imports
import React from "react";
import moment from "moment";

// description
export const GalleryDescription = ({ item }) => (
    <div className="content">
        <h5>{item.title}</h5>
        {item.desc && <p>{item.desc}</p>}
        <div className="stats">
            {
                item.type === "img" &&
                    <React.Fragment>
                        <p className="views">{item.stats.count} images</p>
                    </React.Fragment>
            }
            {
                item.type === "vid" &&
                <React.Fragment>
                    <p className="count">{item.stats.duration} mins</p>
                </React.Fragment>
            }
        </div>
    </div>
);

// thumbnail
export const ThumbnailWrapper = ({ item, click }) => (
    <div className="thumb-wrapper" onClick={click ? click : null}>
        <img
            className={`icon ${item.type === "vid" ? "video" : "img"}`}
            src={require(`@images/logo/${
                item.type === "vid" ? "play-btn" : "img-gallery"
            }.png`)}
            alt="Media Gallery"
        />
        <img
            className="thumb"
            src={item.thumb}
            alt={item.title}
        />
    </div>
);


// views
// <p className="views">
//     {moment([
//         item.stats.date.year,
//         item.stats.date.month - 1,
//         item.stats.date.day
//     ]).fromNow()}
// </p>