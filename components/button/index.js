// imports
import React from "react";
import Link from "next/link";

// styles
import "@styles/components/button.scss";

// quiz button
export const Button = ({ children, classes, href, onClick }) =>
	href ? (
		<Link href={href}>
			<button className={`button raised ${classes ? classes : ""}`} onClick={onClick}>
				{children}
			</button>
		</Link>
	) : (
		<button className={`button raised ${classes ? classes : ""}`} onClick={onClick}>
			{children}
		</button>
	);
