// imports
import { Fragment, useState, useEffect } from "react";
import { Button } from "@components/button";
import cookie from 'js-cookie';

// BoomBox
export const CookieConsent = () => {
	// state
	const [isAccepted, setCookieState] = useState(true);

	function updateCookieConsent(){
		cookie.set('gdpr_cookie_consent', '1');
		setCookieState(true);
	}

	useEffect(() => {
       if( cookie.get('gdpr_cookie_consent') != undefined){
		setCookieState(true);
	   }else{
		setCookieState(false);
	   }
    }, []);
	// render
	return (
		<Fragment>
			{ isAccepted || (
				<div className="consent-wrapper">
					<p>
						We use cookies to ensure you get the best experience. By
						using this website, you agree to the <a href="/privacy" target="_blank">use of cookies</a>.
					</p>
					<div className="button-wrapper">
						<Button classes="long-btn" onClick={updateCookieConsent}>ACCEPT COOKIES</Button>
						<img src={require("@images/logo/close.png")} className="close-btn" onClick={()=>setCookieState(true)}/>
					</div>
				</div>
			)}
		</Fragment>
	);
};
