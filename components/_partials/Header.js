// import
import Head from "next/head";

// component
const Header = () => {
	return (
		<Head>
			<meta charSet="utf-8" />
			<meta
				name="viewport"
				content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
			/>
			<meta httpEquiv="X-UA-Compatible" content="IE=Edge" />
			<meta name="format-detection" content="telephone=no" />
			<link rel="shortcut icon" href="/favicon.ico" />

			<link
				rel="stylesheet"
				href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"
			/>
			<link rel="stylesheet" href="/clientlibs/css/plugins.css" />
			<link rel="stylesheet" href="/clientlibs/css/components.css" />
			<link rel="stylesheet" href="https://use.typekit.net/lhi2oiy.css" />

			{/* ligtning bolt */}
			<script
				id="lightning_bolt"
				type="text/javascript"
				src="//cdn-akamai.mookie1.com/LB/LightningBolt.js"
			/>
			{/* GTM */}
			<script
				dangerouslySetInnerHTML={{
					__html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				      })(window,document,'script','dataLayer','GTM-538D4FF');`
				}}
			/>
			{/* FB: Pixel */}
			<script
				dangerouslySetInnerHTML={{
					__html: `!function(f,b,e,v,n,t,s)
				      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				      n.queue=[];t=b.createElement(e);t.async=!0;
				      t.src=v;s=b.getElementsByTagName(e)[0];
				      s.parentNode.insertBefore(t,s)}(window, document,'script',
				      'https://connect.facebook.net/en_US/fbevents.js');
				      fbq('init', '977185412620356');
				      fbq('track', 'PageView');`
				}}
			/>
			{/* FB: Pixel */}
			<noscript>
				<img
					height="1"
					width="1"
					style={{display: "none"}}
					src="https://www.facebook.com/tr?id=977185412620356&amp;ev=PageView&amp;noscript=1"
				/>
			</noscript>
		</Head>
	);
};

// export
export default Header;
