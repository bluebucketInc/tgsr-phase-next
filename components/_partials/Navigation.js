// imports
import { useState, Fragment, useEffect } from 'react';
import { toast } from "react-toastify";
import {withRouter} from 'next/router';
import cookie from 'js-cookie';
import { getRandomInt } from "@utils";
import axios from "axios";
// components
import { PointsToast, AlertToast } from "@components/toast";
import ProfileCard from "@components/profile-card";
import { LoginPopup, LoginStatus } from "@components/login-screens/Login";
import { jwtEncodeData } from "@components/jwt-verify";

// styles
import 'react-toastify/dist/ReactToastify.css';
import '@styles/navigation.scss';

// Navigation page
const Navigation = ( { router } ) => {

    // login items
    const [isLoginActive, setLoginState] = useState(false);
    const [isLoginStatusActive, setLoginStatusState] = useState(false);
    const [loginStatus, setLoginStatus] = useState('fail');
    // menu card
    const [isCardActive, setCardActive] = useState(false);

    //SessionVars
    const [isLoggedIn, setLoggedIn] = useState(false);
    const [userId, setUserId] = useState(0);
    const [userName, setUserName] = useState('');
    const [fansAhead, setFansAhead] = useState(0);
    const [pointsForTop, setPointsForTop] = useState(0);
    const [weeklyPoints, setWeeklyPoints] = useState(0);
    const [totalPoints, setTotalPoints] = useState(0);
    const [activeweek, setActiveWeek] = useState(0);
    const [signintext, setSignintext] = useState('');
    const [totalFansAhead, setTotalFansAhead] = useState(0);
    const [allPointsForTop, setAllPointsForTop] = useState(0);

    var pinkToastMsg = {
        '/' : {
            'points'    : '10',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Explore a webpage to rack up Riffs! ', 'Find the bonus spin player to earn riffs.'],
        },
        '/artists' : {
            'points'    : '10',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Explore a webpage to rack up Riffs! ', 'Find the bonus spin player to earn riffs.'],
        },
        '/mashup-gallery' : {
            'points'    : '10',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Explore a webpage to rack up Riffs! ', 'Find the bonus spin player to earn riffs.'],
        },
        '/producers' : {
            'points'    : '10',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Explore a webpage to rack up Riffs! ', 'Find the bonus spin player to earn riffs.'],
        },
        '/singapore-finest-artists' : {
            'points'    : '10',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Explore a webpage to rack up Riffs! ', 'Find the bonus spin player to earn riffs.'],
        },
        '/times-tunes' : {
            'points'    : '10',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Explore a webpage to rack up Riffs! ', 'Find the bonus spin player to earn riffs.'],
        },
        '/events' : {
            'points'    : '10',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Explore a webpage to rack up Riffs! ', 'Find the bonus spin player to earn riffs.'],
        },
        '/gamification' : {
            'points'    : '10',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Explore a webpage to rack up Riffs! ', 'Find the bonus spin player to earn riffs.'],
        },
        '/media-gallery' : {
            'points'    : '30',
            'pre_login' : ['Sign in to rack up Riffs and win amazing prizes!'],
            'post_login': ['Catch up on the episodes to rack up Riffs!', 'View an album to rack up Riffs!'],
        },
    };

    var currentPage     = router.asPath;
    var currentPageTemp = router.asPath;

    if(currentPage != "/") {
        currentPage = currentPage.toString().replace(/\/$/, '');
    } else {
        currentPageTemp = 'home';
    }

    currentPageTemp = currentPageTemp.replace('/', '').replace('-', '_');

    function onCloseToast()
    {
        let d = new Date();
        d.setTime(d.getTime() + (5*60*1000));
        if(currentPageTemp == 'media_gallery'){
            cookie.set('pink_toast_media_gallery', '1', {expires: d});
        }else{
            cookie.set('pink_toast_notify', '1', {expires: d});
        }
    }

    function getPinkToast(loginType)
    {
        if(typeof pinkToastMsg[currentPage] != 'undefined' && ((currentPageTemp != 'media_gallery' && cookie.get('pink_toast_notify') == undefined) || (currentPageTemp == 'media_gallery' && cookie.get('pink_toast_media_gallery') == undefined)))
        {
            let rand = getRandomInt(0, pinkToastMsg[currentPage][loginType].length - 1);
            let forPoints = pinkToastMsg[currentPage]['points'];

            if(currentPage == '/media-gallery' && loginType == 'post_login'){
                const tempRand = getRandomInt(0, 9);
                if(tempRand < 7){
                    rand = 0;
                    forPoints = 30;
                }else{
                    rand = 1;
                    forPoints = 20;
                }
            }else if(loginType == 'post_login' && rand == 1){
                forPoints = 100;
            }

            // To get random message from list
            var message = pinkToastMsg[currentPage][loginType][rand];

            if(loginType == 'pre_login')
            {
                toast(<AlertToast  autoClose={false} type='signin' showMessage={message} />, {
                    onClose: () => onCloseToast(), autoClose: false
                });
            }
            else if(loginType == 'post_login')
            {
                toast(<AlertToast  autoClose={false} points={forPoints} showMessage={message} />, {
                    onClose: () => onCloseToast(), autoClose: false
                });
            }
        }
    }

    useEffect(() => {
        window.isScrolled = 0;
        async function anyNameFunction() {
            $(window).on('scroll', function () {
                if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.5 && window.isScrolled == 0){
                    window.isScrolled = 1;
                    var data = {
                        "event_type"    : "view_webpage",
                        "action_element": router.asPath
                    };

                    axios(process.env.REACT_APP_API_URL + 'add-points?date=' + Math.floor(Date.now() / 1000), {
                        method: 'post',
                        headers: {
                        'Accept': 'application/json, text/plain, */*',
                        'Content-Type': 'application/json'
                        },
                        data: jwtEncodeData(data)
                    }).then((res) => {
                        var response = res.data;
                        if(response.status == 1){
                            checkLogin();
                            toast(<PointsToast points={response.points} eventMessage={response.event_msg} />);
                        }

                    });
                }
            });

            if($("body").height() <= $(window).height()){
                window.isScrolled = 1;
                var data = {
                    "event_type"    : "view_webpage",
                    "action_element": router.asPath
                };

                axios(process.env.REACT_APP_API_URL + 'add-points?date=' + Math.floor(Date.now() / 1000), {
                    method: 'post',
                    headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                    },
                    data: jwtEncodeData(data)
                }).then((res) => {
                    var response = res.data;
                    if(response.status == 1){
                        checkLogin();
                        toast(<PointsToast points={response.points} eventMessage={response.event_msg} />);
                    }

                });
            }

            const response = await axios.get(process.env.REACT_APP_API_URL + 'check-login?date=' + Math.floor(Date.now() / 1000))
            const auth_response = response.data;
            if(auth_response.is_logged_in == 1){
                setLoggedIn(auth_response.is_logged_in);
                setUserId(auth_response.user_id);
                setUserName(auth_response.user_name);
                setFansAhead(auth_response.fans_ahead)
                setPointsForTop(auth_response.points_for_top);
                setWeeklyPoints(auth_response.weekly_points);
                setTotalPoints(auth_response.total_points);
                setActiveWeek(auth_response.activeweek);
                setTotalFansAhead(auth_response.total_fans_ahead);
                setAllPointsForTop(auth_response.all_points_for_top);
                getPinkToast('post_login');
            }else{
                setSignintext('Sign In');
                setLoggedIn(auth_response.is_logged_in);
                getPinkToast('pre_login');
            }
        }
        anyNameFunction();
    }, []);

    var loginTypeSpanCSS = {
        'display'  : 'none',
        'visiblity': 'none',
    };

    // render
    return (
        <Fragment>
            {/* login poups */}
            {
                isLoginActive && <LoginPopup onClose={() => setLoginState(false)} handleSocialLogin={handleSocialLogin} handleSocialLoginFailure={handleSocialLoginFailure} />
            }
            {
                isLoginStatusActive && <LoginStatus status={loginStatus} onClose={() => setLoginStatusState(false)} onContinue={() => setLoginStatusState(false)} />
            }

            <div className="tgsr-logo">
                <a href="/#">
                    <img src="/clientlibs/images/global/tgsr_logo.gif" />
                </a>
            </div>
            <header>
                <ul className="nav-bar">
                    <li className="profile"> {
                        !isLoggedIn
                            ? <img src={require("@images/navigation/icon_profile@2x.png")} width="50%" onClick={() => setLoginState(true)} />
                            : <img src={require("@images/navigation/icon_profile@2x.png")} width="50%" onClick={() => setCardActive(!isCardActive)} />
                        }
                        <span style={loginTypeSpanCSS} id="login_type">{isLoggedIn ? 'post' : 'pre'}</span>
                    </li>
                    {
                            !isLoggedIn ? <li className="sign-in" onClick={() => setLoginState(true)}>{signintext}</li>
                        :
                            <li className="profile-item">
                                <div>
                                    <div className="inner-content" onClick={() => setCardActive(!isCardActive)}>
                                    {
                                        activeweek > 0 ?
                                        <Fragment>
                                        <span className="week">This week:</span>
                                        <span className='riffs'><span id="wriffs">{weeklyPoints}</span> riffs<img src={require("@images/navigation/icon_riff_white@2x.png")} /></span>
                                        </Fragment>
                                        :
                                        <Fragment>
                                        <span className="week">All weeks:</span>
                                        <span className='riffs'><span id="wriffs">{totalPoints}</span> riffs<img src={require("@images/navigation/icon_riff_white@2x.png")} /></span>
                                        </Fragment>
                                    }
                                    </div>
                                    <img
                                        onClick={() => setCardActive(!isCardActive)}
                                        src={require(`@images/navigation/icon_arrow_${isCardActive ? "up" : "down"}@2x.png`)}
                                        className="arrow-trigger"
                                        alt="arrow"
                                    />
                                    { isCardActive && <ProfileCard data={{ fans: fansAhead, riffstogo: pointsForTop, riffs: totalPoints, activeweek: activeweek, totalFans: totalFansAhead, totalRiffstogo: allPointsForTop }} onLogout={doLogout} /> }
                                </div>
                            </li>
                    }

                    <li className ="share-icon">
                        <div className="share-icon">
                            <img src="/clientlibs/images/navigation/icon_share@2x.png" />
                            <div className="social-share-link">
                                <a
                                    href="https://www.facebook.com/sharer.php?u={url}"
                                    title="facebook"
                                    target="_blank"
                                >
                                    <img src="/clientlibs/images/global/icon-facebook@2x.png" />
                                </a>
                                <a
                                    href="https://twitter.com/intent/tweet?url={url}"
                                    title="Twitter"
                                    target="_blank"
                                >
                                    <img src="/clientlibs/images/global/icon-twitter@2x.png" />
                                </a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div className="menu-icon">
                            <div className="bars">
                                <div className="bar-1" />
                                <div className="bar-2" />
                                <div className="bar-3" />
                            </div>
                        </div>
                    </li>
                </ul>
                <div className="nav-menu">
                    <div className="navbar-anchors">
                        <div className="tgsr-logo-inside">
                            <a href="/"><img src="/clientlibs/images/global/tgsr_logo.gif" /></a>
                        </div>
                        <ul className="main-links">
                            <li><a className="hamburger" href="/">HOME</a></li>
                            <li><a href="/gamification" id="gamification">RACK UP RIFFS</a></li>
                            <li><a href="/showcase-concert" id="festival">SHOWCASE CONCERT</a></li>
                            <li><a href="/artists" id="artists">ASPIRING MUSIC TALENTS</a></li>
                            <li><a href="/mashup-gallery" id="mashups">FAVORITE MASHUPS</a></li>
                            <li><a href="/producers" id="producers">PRODUCERS</a></li>
                            <li><a href="/media-gallery" id="replaystudio">REPLAY STUDIO</a></li>
                            <li><a href="/singapore-finest-artists" id="establishedartists">ESTABLISHED ARTISTS</a></li>
                            <li><a href="/spin-player/index.html" id="spinplayer">SPIN PLAYER</a></li>
                            <li><a href="/times-tunes" id="timesntunes">TIMES & TUNES</a></li>
                            <li><a className="events" href="/events">EVENTS</a></li>
                        </ul>
                        <ul className="other-links">
                            <li><a href="/about" id="abouthtml">ABOUT THE PROGRAMME</a></li>
                            <li><a href="/alumni" id="thealumni">THE ALUMNI</a></li>
                            <li><a href="https://stories.thegreatsingaporereplay.sg/" target="_blank" id="replay">REPLAY STORIES</a></li>
                            <li><a href="/faq" id="faqs">FAQs</a></li>
                            <li><a href="/tnc" target="_blank" id="termscondition">TERMS &amp; CONDITIONS</a></li>
                            <li><a href="/privacy" target="_blank" id="privacy">PRIVACY POLICY</a></li>
                            <li className="social-icons-list">
                                <a id="facebook_social" href="https://www.facebook.com/thegreatsingaporereplay/">
                                    <img src="/clientlibs/images/global/icon_fb@2x.png" />
                                </a>
                                <a id="instagram_social" href="https://www.instagram.com/thegreatsingaporereplay/">
                                    <img src="/clientlibs/images/global/icon_ig@2x.png" />
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="navigation-background">
                        <img className="for-desktop" src="/clientlibs/images/navigation/nav-background.jpg" />
                        <img className="for-mobile" src="/clientlibs/images/navigation/nav-background-mobile.jpg" />
                    </div>
                    <div className="spot-lights">
                        <div className="spot-light-left static">
                            <img src="/clientlibs/images/lights/img_spotlight_faded@2x.png" />
                        </div>
                        <div className="spot-light-right static">
                            <img src="/clientlibs/images/lights/img_spotlight_faded@2x.png" />
                        </div>
                    </div>
                    <div className="bottom-background">
                        <a className="no-line" href="https://www.temasek.com.sg/" target="_blank">
                            <img src="/clientlibs/images/navigation/bg_hero_nav@2x.png" />
                        </a>
                    </div>
                </div>
            </header>
            <div className="spot-lights body-contents spin-player-page">
                <div className="spot-light-left infinite">
                    <img src="/clientlibs/images/lights/img_spotlight_faded@2x.png" />
                </div>
                <div className="spot-light-right infinite">
                    <img src="/clientlibs/images/lights/img_spotlight_faded@2x.png" />
                </div>
            </div>
        </Fragment>
    );

    function handleSocialLoginFailure(err){
        console.error(err);
        setLoginState(false);
        setLoginStatus('fail');
        setLoginStatusState(true);
    }

    function handleSocialLogin(user){
        var data = {
            "social_type": user._provider,
            "social_id": user._profile.id,
            "email_id": user._profile.email,
            "full_name": user._profile.name
        }
        axios(process.env.REACT_APP_API_URL + 'login', {
                method: 'post',
                headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
                },
                data: jwtEncodeData(data)
            }).then((res) => {
                //toast.dismiss();
                axios.get(process.env.REACT_APP_API_URL + 'check-login?date=' + Math.floor(Date.now() / 1000)).then((response) => {
                    var auth_response = response.data;
                    if(auth_response.is_logged_in == 1){
                        setLoggedIn(auth_response.is_logged_in);
                        setUserId(auth_response.user_id);
                        setUserName(auth_response.user_name);
                        setFansAhead(auth_response.fans_ahead)
                        setPointsForTop(auth_response.points_for_top);
                        setWeeklyPoints(auth_response.weekly_points);
                        setTotalPoints(auth_response.total_points);
                        setActiveWeek(auth_response.activeweek);
                        setTotalFansAhead(auth_response.total_fans_ahead);
                        setAllPointsForTop(auth_response.all_points_for_top);
                        jQuery(".rsvpBtnToDo").trigger("click");
                        if(jQuery(".rsvpBtnToDo").length < 1){
                            jQuery("#triggerRsvp").trigger("click");
                        }
                        setLoginState(false);
                        setLoginStatus('success');
                        setLoginStatusState(true);
                        getPinkToast('post_login');
                    }else{
                        setSignintext('Sign In');
                        setLoggedIn(auth_response.is_logged_in);
                        setLoginState(false);
                        setLoginStatus('fail');
                        setLoginStatusState(true);
                        getPinkToast('pre_login');
                    }

                });
        });
    }

    function doLogout(){
        axios(process.env.REACT_APP_API_URL + 'logout?date=' + Math.floor(Date.now() / 1000)).then((res) => {
            axios.get(process.env.REACT_APP_API_URL + 'check-login?date=' + Math.floor(Date.now() / 1000)).then((response) => {
                var auth_response = response.data;
                if(auth_response.is_logged_in == 1){
                    setLoggedIn(auth_response.is_logged_in);
                    setUserId(auth_response.user_id);
                    setUserName(auth_response.user_name);
                    setFansAhead(auth_response.fans_ahead)
                    setPointsForTop(auth_response.points_for_top);
                    setWeeklyPoints(auth_response.weekly_points);
                    setTotalPoints(auth_response.total_points);
                    setActiveWeek(auth_response.activeweek);
                    setTotalFansAhead(auth_response.total_fans_ahead);
                    setAllPointsForTop(auth_response.all_points_for_top);
                }else{
                    setSignintext('Sign In');
                    setLoggedIn(auth_response.is_logged_in);
                    jQuery("#triggerRsvp").trigger("click");
                }

            });
        });

        setCardActive(false);
    }

    function checkLogin(){
        axios.get(process.env.REACT_APP_API_URL + 'check-login?date=' + Math.floor(Date.now() / 1000)).then((response) => {

            var auth_response = response.data
            if(auth_response.is_logged_in == 1){
                setLoggedIn(auth_response.is_logged_in);
                setUserId(auth_response.user_id);
                setUserName(auth_response.user_name);
                setFansAhead(auth_response.fans_ahead)
                setPointsForTop(auth_response.points_for_top);
                setWeeklyPoints(auth_response.weekly_points);
                setTotalPoints(auth_response.total_points);
                setActiveWeek(auth_response.activeweek);
                setTotalFansAhead(auth_response.total_fans_ahead);
                setAllPointsForTop(auth_response.all_points_for_top);
            }else{
                setSignintext('Sign In');
                setLoggedIn(auth_response.is_logged_in);
            }

        });
    }
};

// export
export default withRouter(Navigation);
