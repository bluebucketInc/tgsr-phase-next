
// imports
import { ToastWrapper } from "@components/toast";
import { CookieConsent } from "@components/cookie-consent";

// Footer Component
const Footer = () => {
	return (
		<>
			{/* Toast Container */}
            <ToastWrapper />
            {/* Cookie Consent */}
            <CookieConsent />
			<footer className="body-contents relative">
				<div className="container-fluid">
					<div className="row">
						<div className="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3">
							<ul className="footer-list footer-left">
								<li>
									<div className="title">Presented by</div>
									<div className="partner-logo">
										<a
											className="no-line"
											href="https://www.temasek.com.sg/"
											target="_blank"
										>
											<img src="/clientlibs/images/global/logo_temasek_white@2x.png" />
										</a>
									</div>
								</li>
							</ul>
						</div>
						<div className="col-12 col-sm-8 col-md-9 col-lg-9 col-xl-9">
							<div className="footer-logos-block">
								<div className="footer-logos-content">
									<img src="/clientlibs/images/footer/footer-logo.png" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<script src="/clientlibs/js/plugins/jquery.js" />
			<script src="/clientlibs/js/plugins/bootstrap.min.js" />
			<script src="/clientlibs/js/plugins/slick.js" />
			<script src="/clientlibs/js/plugins/aos.js" />
			<script src="/clientlibs/js/plugins/dateformat.js" />
			<script src="/clientlibs/js/scripts.js" />
		</>
	);
};

// export
export default Footer;
