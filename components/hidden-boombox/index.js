// imports
import { Fragment, useState } from "react";
import { toast } from "react-toastify";
import { PointsToast, AlertToast } from "@components/toast";
import { jwtEncodeData } from "@components/jwt-verify";
import { getRandomInt } from "@utils";

import "@styles/boombox.scss";
// BoomBox
export const Boombox = ({ type, onClick, className, bbID, state }) => {
	// set active
	const [isActive, setActive] = useState(false);
	const [newState, setNewState] = useState('');

	function boomboxClicked() {
		var request ={
			"event_type"    : "boombox",
			"action_element": bbID
		}
		fetch(process.env.REACT_APP_API_URL + 'add-points', {
			method: 'post',
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: jwtEncodeData(request)
		}).then((res) => {
			res.json().then((response) => {
				if(response.status == 1){
					setNewState('click');
					setActive(true);
					$('#wriffs').html(parseInt($('#wriffs').html()) + response.points);
					// toast(<PointsToast points={response.points} eventMessage={response.event_msg} dynamicMessage={response.dynamic_msg} />);
				}else if(response.status == 2){
					setNewState('earn');
					setActive(true);
				}
			});
		});
	}

	// rand
	const rand = getRandomInt(0, 2);

	// render
	return (
			<Fragment>
			{type === "correct" ? (
				<div className={`hidden-boombox-wrapper ${className}`} >
				<div className="correct-wrapper" onClick={() => boomboxClicked()}>
					{
						newState === "earn"
						? 	<img
								className="bg"
								src={require(`@images/boombox/correct-${isActive ? "on_earn" : "off"}.png`)}
								alt="BoomBox"
							/>
						:
							<img
								className="bg"
								src={require(`@images/boombox/correct-${isActive ? `on_click_${rand}` : "off"}.png`)}
								alt="BoomBox"
							/>
					}
					<img
						className="gif"
						src={require(`@images/boombox/boombox_animation.gif`)} alt="BoomBox"
					/>
				</div>
				</div>
			) : (
				<div className={`hidden-boombox-wrapper ${className}`}  onClick={() => setActive(true)}>
				<img
					src={require(`@images/boombox/wrong-${isActive ? ("on-" + rand) : "off"}.png`)}
					alt="BoomBox"
				/>
				</div>
			)}
		</Fragment>
	);
};
