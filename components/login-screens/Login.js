// imports
import { Fragment, useState } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
// components
import { Button } from "@components/button";
// styles
import "@styles/login-overlay.scss";
import SocialButton from "../social/SocialButton";
// login popup
export const LoginPopup = ({
    onClose,
    handleSocialLogin,
    handleSocialLoginFailure
}) => {

    var noPointer = {
        'pointerEvents': 'none'
    }

    var blankStyle = {};

    // state
    const [isChecked, setAge] = useState(false);
    // render
    return (
        <div className="overlay">
            <div className="overlay-content">
                <div className="closebtn">
                    <img
                        src={require("@images/navigation/icon_close_white.png")}
                        alt="close-btn"
                        onClick={onClose}
                    />
                </div>
                <div className="sign-in">
                    <h4 className="text-uppercase">
                        Ready for stardom?
                    </h4>
                    <p className="signin-text">
                    Sign in using your Google or Facebook account to begin racking up Riffs! 
                    </p>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={isChecked}
                                onChange={(e) => setAge(e.target.checked)}
                                value="age"
                            />
                        }
                        label="I am 13 years old and above."
                    />
                    <div className="btn-sec">
                        <div className="btn-sign-in" style={isChecked ? blankStyle : noPointer } >
                            <img
                                src={require("@images/navigation/btn_icon_fb.png")}
                                alt="sign in with Facebook"
                            />
                            <SocialButton
                                provider="facebook"
                                appId={process.env.REACT_APP_FACEBOOK_APP_ID}
                                onLoginSuccess={handleSocialLogin}
                                onLoginFailure={handleSocialLoginFailure}
                                classes="text-uppercase fb"
                            >
                                Sign In with Facebook
                            </SocialButton>
                        </div>
                        <div className="btn-sign-in" style={isChecked ? blankStyle : noPointer } >
                            <img
                                src={require("@images/navigation/btn_icon_google.png")}
                                alt="sign in with Google"
                            />
                            <SocialButton
                                provider="google"
                                appId={process.env.REACT_APP_GOOGLE_APP_ID}
                                onLoginSuccess={handleSocialLogin}
                                onLoginFailure={handleSocialLoginFailure}
                                classes="text-uppercase google"
                            >
                                Sign In with Google
                            </SocialButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

// login status
export const LoginStatus = ({ status, onClose, onContinue }) => {
    return (
        <div className="overlay">
            <div className="overlay-content">
                <div className="closebtn">
                    <img
                        src={require("@images/navigation/icon_close_white.png")}
                        alt="clost btn"
                        onClick={onClose}
                    />
                </div>
                <div className="sign-in">
                    {status === "fail" ? (
                        <Fragment>
                            <h4 className="text-uppercase">OOPS…</h4>
                            <h4 className="text-uppercase">
                            struck a wrong chord!
                            </h4>
                            <p className="signin-text">
                            Something went wrong...check your details and try again! Fields are case-sensitive.
                            </p>
                            <div className="bottom-wrapper">
                                <Button classes="long-btn" onClick={onContinue}>
                                    Try Again
                                </Button>
                            </div>
                        </Fragment>
                    ) : (
                        <Fragment>
                            <img
                                src={require("@images/navigation/img_successful_boombox.png")}
                                alt="BoomBox"
                            />
                            <h3 className="text-uppercase successful">
                            It's time to rock 'n' roll! 
                            </h3>
                            <p className="signin-text">
                            Let the jam session begin! Explore the site for exclusive updates, news, activities, and more! 
                            </p>
                            <div className="bottom-wrapper">
                                <Button classes="long-btn" onClick={onContinue}>
                                    Continue
                                </Button>
                            </div>
                        </Fragment>
                    )}
                </div>
            </div>
        </div>
    );
};
