// imports
const path = require('path');
const withImages = require("next-images");
const withSass = require("@zeit/next-sass");
const withCSS = require('@zeit/next-css')
const withPlugins = require("next-compose-plugins");
const { artists } = require('./assets/content/artists.json');
const { parsed: localEnv } = require('dotenv').config()
const webpack = require('webpack');

// next config
const nextConfig = {
	exportTrailingSlash: true,
	webpack(config, options) {
		// aliases
		config.resolve.alias["@components"] = path.join(__dirname, "components");
		config.resolve.alias["@styles"] = path.join(__dirname, "assets/styles");
		config.resolve.alias["@images"] = path.join(__dirname, "assets/static/images");
		config.resolve.alias["@content"] = path.join(__dirname, "assets/content");
		config.resolve.alias["@utils"] = path.join(__dirname, "libs/utils");
		// config
		config.plugins.push(new webpack.EnvironmentPlugin(localEnv));
		// polyfill
		const originalEntry = config.entry;
		config.entry = async () => {
			const entries = await originalEntry();

			if (
				entries["main.js"] &&
				!entries["main.js"].includes("./libs/polyfills.js")
			) {
				entries["main.js"].unshift("./libs/polyfills.js");
			}

			return entries;
		};
		return config;
	},
	exportPathMap: function() {
		// page paths
		const paths = {
			"/producers": { page: "/producers" },
			"/artists": { page: "/artists" },
			"/media-gallery": { page: "/media-gallery" },
			"/about"			: { page: "/about" },
			"/alumni"			: { page: "/alumni" },
			"/faq"				: { page: "/faq" },
			"/tnc"				: { page: "/tnc" },
			"/privacy"			: { page: "/privacy" },
			"/events"			: { page: "/events" },
			"/times-tunes"		: { page: "/times-tunes" },
			"/showcase-concert"			: { page: "/showcase-concert" },
			"/singapore-finest-artists"	: { page: "/singapore-finest-artists" },
			"/mashup-gallery": { page: "/mashup-gallery" },
			"/qr-page": { page: "/qr-page" },
			"/spin-player": { page: "/spin-player" },
			"/": { page: "/" }
		};

		// dynamic pages
		artists.forEach(artist => {
			paths[`/artists/${artist.id}`] = { page: '/artists/[id]', query: { id: artist.id } };
		});

		// final path
		return paths;
	},
};

// export
module.exports = withPlugins([[withImages, {}], [withCSS, {}], [withSass, {}]], nextConfig);