// Header JS
(function(){
    function _menuHandler(){
        $('header').toggleClass('active');
        $('.body-contents').toggleClass('content-toggle');
        $('body').toggleClass('background-toggle');
        $('.tgsr-logo').toggleClass('menu-triggered');
        $(['.music-icon','.share-icon']).toggleClass('hide');
        // $('.spot-lights').toggleClass('menu-triggering');
    }

    function _addEvents(){
        $('.menu-icon').click(_menuHandler)
    }
    function _toggleSound(){
            var myAudio = document.getElementById('notifypop');
            if(!$('.music-icon').hasClass('paused')) {
               myAudio.play();
               myAudio.loop = true; 
            }
              else {
                myAudio.pause();
                myAudio.loop = true; 
            }
    }
    $('.music-icon').click(function(){
       _toggleSound();
    })

    function _init(){
        _addEvents();
    }
    $(_init());
})(jQuery);


// Social Share Component 
const links = document.querySelectorAll('.social-share-link a');

function onClick(event) {
    event.preventDefault();
    window.open(
    event.currentTarget.href,
        'Поделиться',
        'width=600,height=500,location=no,menubar=no,toolbar=no'
    );
}

$.each(links, function(i, link){
    const url = encodeURIComponent(window.location.origin + window.location.pathname);
    // const title = encodeURIComponent(document.title);

    link.href = link.href.
    replace('{url}', url);
    // replace('{title}', title);

    link.addEventListener('click', onClick);
});




// (function(d, s, id) {
//     var js, fjs = d.getElementsByTagName(s)[0];
//     if (d.getElementById(id)) return;
//     js = d.createElement(s); js.id = id;
//     js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
//     fjs.parentNode.insertBefore(js, fjs);
// }(document, 'script', 'facebook-jssdk'));


