(function ($) {

    var _$dropdown, _$listItem, _accordianList;

    function _dropdownHandler(e) {
        e.stopPropagation();

        $(e.currentTarget).toggleClass("active");
        $(e.currentTarget).next().toggleClass("active");
    }

    function _listItemClickHandler(e) {
        e.preventDefault();
        var $item = $(e.currentTarget);
        _$listItem.removeClass("active");
        $item.addClass("active");

        //for responsive dropdown widget...
        $item.closest("ul").removeClass("active");
        _$dropdown.removeClass("active");
        _$dropdown.find("span").html($item.text());

        _hashChangeHandler($item.attr("href"));
    }

    function _hashChangeHandler(location) {
        location = location.replace(/\#/g, "");
        if (location) {
            _accordianList.hide().filter(function (i, item) {
                return $(item).attr("id").indexOf(location) > -1;
            }).fadeIn();
        }
    }

    function _variables() {
        _$dropdown = $(".dropdown");
        _$listItem = $(".faq-list li a");
        _accordianList = $(".accordion.list");

        $.fn.randomize = function (selector) {
            var $elems = selector ? $(this).find(selector) : $(this).children(),
                $parents = $elems.parent();

            $parents.each(function () {
                $(this).children(selector).sort(function () {
                    return Math.round(Math.random()) - 0.5;
                }).detach().appendTo(this);
            });

            return this;
        };
    }

    function _addEvents() {
        _$dropdown.on("click", _dropdownHandler);
        _$listItem.on("click", _listItemClickHandler);
        //$(window).on("click", _hashHandler);
    }

    function _hashHandler() {
        if (event.target.getAttribute('href')) {
            var location = event.target.getAttribute('href').replace(/\#/g, "");
            _hashChangeHandler(location);
        }
    }

    function _init() {
        _variables();
        _addEvents();
    }

    $(_init());
})(jQuery);
