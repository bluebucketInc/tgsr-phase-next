(function ($) {

    let _eventCarousel, _eJSON, _lazyLoadingOffset = 350;


    function _slickInitHandler(e) {
        _eventCarousel.fadeIn();
        setTimeout(function(){
            _preparecontent("UPCOMING", _eJSON.upcoming);
        }, 500)

    }

    function _eventsTabHandler(e){
        e.preventDefault();
        $(".tabs-row a").removeClass("active");
        $(this).addClass("active");

        var selectedTab = $(this).attr("data-id");
        _preparecontent(selectedTab.toUpperCase(), _eJSON[selectedTab]);
    }

    function _readMoreHandler(e) {
        e.preventDefault();
        $(".events-container").fadeOut();
        var dataId = $(e.target).attr("id");
        var section = dataId.toString().split("_")[0];
        var index = parseInt(dataId.toString().split("_")[1]);
        var data = _eJSON[section][index];
        var $popover = $(".popover");



        // $popover.find(".event-slide").find(".title-pink").text(section.toUpperCase());
        $popover.find(".event-slide").find(".title-pink").text(section.toUpperCase());
        $popover.find(".event-slide").find("img").attr("src", data.image);
        $popover.find(".event-slide").find(".title-blue").text(data.title);
        $popover.find(".event-slide").find(".date").text(data.date);
        $popover.find(".event-slide").find(".place").text(data.venue);
        $popover.find(".event-slide").find(".details.description").html(data.overlayDescription);


        $("html, body").animate({scrollTop: 0}, 0);
        $popover.fadeIn();
    }
    function _closeOverlayHandler() {
        $(".events-container").show();
    }

    function _addEvents() {

        _eventCarousel.on("init", _slickInitHandler);

        $(document).on("click", ".tabs-row a", _eventsTabHandler);
        $(document).on("click", "a.read-more", _readMoreHandler);
        $(document).on("click", ".close-overlay", _closeOverlayHandler);
    }

    function _initializeCarousal() {
        _eventCarousel.slick({
            // centerMode: true,
            // centerPadding: '60px',
            dots: true,
            slidesToShow: 1,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    function _variablesInitialize() {
        _eventCarousel = $('.events-slider');

        if(location.pathname == "/" || location.pathname.toLocaleLowerCase() == "/index.html"){

            $(".tgsr-logo.home-carousel-active").find("img").attr("src", "/clientlibs/images/global/tgsr_logo.gif");
        } else {
            $(".tgsr-logo.home-carousel-active").removeClass("home-carousel-active")
        }
    }

    function _getEventTemplate(heading){
        return $('<li><div class="content">\n' +
            '               <div class="event-tag">'+ heading.toUpperCase() +'</div>\n' +
            '               <h5 class="title-blue">The Great Singapore Replay Spin Player Comes To Town</h5>\n' +
            '               <div class="event-schedule">\n' +
            '                   <div class="schedule date">12 - 14 Nov 2019</div>\n' +
            '                   <div class="schedule place">Raffles City Shopping Center</div>\n' +
            '               </div>\n' +
            '               <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua dignissim per, habeo iusto primis ea eam.</p>' +
            '            </div></li>');
            //  '               <a href="#" class="read-more">Read more</a>\n' +
    }

    function _preparecontent(heading, data){
        if(!data){
            return;
        }

        var $content, $container = $(".events-timeline").empty();
        var $timeLineContainer, $ul = $('<ul></ul>');

        $.each(data, function(i, item){

            $content = _getEventTemplate(heading);
            $content.attr("data-aos", "fade-up");
            $content.attr("data-aos-duration", $(document).width()<765? "300":"600");
            $content.attr("data-aos-offset", _lazyLoadingOffset);
            $content.find(".event-tag").text(item.tag);
            $content.find(".title-blue").text(item.title);
            $content.find(".date").text(item.date);
            $content.find(".place").text(item.venue);
            $content.find("p").text(item.description);
            $content.find(".read-more").attr("id", heading.toLowerCase()+"_"+i);
            $ul.append($content.clone());
        });

        if(heading.toLowerCase() === "upcoming"){
            $timeLineContainer = $('<div class="upcoming-timeline active"></div>');
        } else {
            $timeLineContainer = $('<div class="past-timeline active"></div>');
        }
        $timeLineContainer.append($ul);
        $container.append($timeLineContainer);
    }

    function _parseLocalDate(item){
        var startDate = new Date(item.startDate);
        var endDate = new Date(item.endDate);

        var date = dateFormat(endDate, "d mmm yyyy");

        if(startDate < endDate){
            date = dateFormat(startDate, "d mmm") + " - " + date;
        }

        return date;
    }
    function _populateEventCarousal(data) {
        if(data.length>0){
            var $eventSlider = $(".events-carousel-container .events-slider").empty();
            var $sliderItem = $('<div class="slider-item">\n' +
                '            <div class="events-slider-item">\n' +
                '              <div class="item-content-block">\n' +
                '                <div class="image-block"><img src="clientlibs/images/events/events-banner-1.png"></div>\n' +
                '                <div class="text-block">\n' +
                '                  <div class="text-content">\n' +
                '                    <h5 class="title-pink">THIS MONTH</h5>\n' +
                '                    <h3>THE GREAT SINGAPORE REPLAY<br>SPIN PLAYER COMES TO TOWN</h3>\n' +
                '                    <div class="event-details">\n' +
                '                      <div class="event-item event-date">12 - 15 Nov 2019</div>\n' +
                '                      <div class="event-item event-place">Raffles City Shopping Centre</div>\n' +
                '                    </div>\n' +
                '                    <div class="expanding-btn"><a class="read-more" href="#" id="upcoming_1" tabindex="0">FIND OUT MORE</a></div>\n' +
                '                  </div>\n' +
                '                </div>\n' +
                '              </div>\n' +
                '            </div>\n' +
                '          </div>');

            $.each(data, function (i, item) {
                $sliderItem.find("h3").text(item.title);
                $sliderItem.find(".date").text(item.date);
                $sliderItem.find(".place").text(item.venue);
                $sliderItem.find(".expanding-btn").attr("id", "current_"+i);
                $sliderItem.find(".image-block img").attr("src", item.poster);
                $eventSlider.append($sliderItem);
            });


        }

        _initializeCarousal();
    }

    function _populateContent(){
        $.getJSON("/clientlibs/events.json", function(response){

            _eJSON = {
                current: [],
                upcoming: [],
                past: []
            };

            var startDate, endDate, currentDate= new Date();

            $.each(response.data, function (i, item) {
                startDate = new Date(item.startDate);
                endDate = new Date(item.endDate);

                var tmpEndDate = new Date(item.endDate);
                tmpEndDate.setDate(endDate.getDate()+1);


                if(currentDate >= startDate && currentDate < tmpEndDate){
                    //current event(s)
                    _eJSON.upcoming.push(item);
                } else if(currentDate < startDate){
                    //upcoming event(s)
                    _eJSON.upcoming.push(item);
                } else if(currentDate >= tmpEndDate){
                    //Past event(s)
                    _eJSON.past.push(item);
                }
                item.date = _parseLocalDate(item)
            });

            _populateEventCarousal(_eJSON.current);
        })
    }

    function _init() {
        _variablesInitialize();
        _addEvents();
        _populateContent();
        //_initializeCarousal();
    }

    $(_init());
})(jQuery);

