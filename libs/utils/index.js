/* global window */

// get URL param
export const getURlParam = name => {
	const url = window.location.href;
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	let regexS = "[\\?&]" + name + "=([^&#]*)";
	let regex = new RegExp(regexS);
	let results = regex.exec(url);
	return results == null ? null : results[1];
};

// get random number
export const getRandomInt = (min, max) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};
